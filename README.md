# MDP-Policy-Transparency
Note this project uses Fast Downward planner as a subroutine, the original unmodified version fo the planner can be found [here](http://www.fast-downward.org/)

## Requirements
* python >= 3.5
* linux (Have only tested on ubuntu 14.0.4)
## Installation steps
* Install all requirements listed in requirements file under src/requirements (can be done by running python -r requirements)
* Install the psummarizer module lister under src/subgoal_generator/policy_summarization/ (python setup.py develop)
* Build the original version of the Fast-Downward planner (MDP-Policy-Transparency-Sarath/other_tools/Fast-Downward) and the modified version (MDP-Policy-Transparency-Sarath/other_tools/FD_LAND_DUMP)
* * The original version of the planners were taken from [here](http://www.fast-downward.org/) 
* * To build the original version please go to the directory and run the command `./build.py`
* * To build the modified version please go to the directory and run the command `./build.py release64`
* This should finish the installation steps
## Running the server
* The system requires both the backend server for the policyviewer system and the subgoal generator to be running. These can be started by running the app.py scripts in their respective folders/
