;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Go to your garrage
;; if your car is working take it to bus stop
;; else take a taxi
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain get_to_sf)
  (:requirements :probabilistic-effects :conditional-effects :negative-preconditions :equality :typing)
  (:types )
  (:constants )
  (:predicates (at_home)
                (at_garrage) (at_busstop)
                (car_running) (need_taxi_to_busstop)
                (at_busstop) (need_to_find_bus)
                (bus_on_time) (bus_missed)
                (at_station1) (at_station2) (at_station3) (at_airport) (on_board_train) (missed_train)
                (at_north_gate) (at_gate_10)
                (has_ticket) (boarded_plane) )

  (:action walk_to_garage
    :parameters ()
    :precondition (and (at_home) )
    :effect (and (at_garrage)  (not (at_home))
            (probabilistic 1/2 (and (car_running))
                           1/2 (and (need_taxi_to_busstop)))
            )
  )

(:action drive_to_busstop
    :parameters ()
    :precondition (and (car_running) (at_garrage))
    :effect (and (at_busstop) (need_to_find_bus) (not (at_garrage)) (not (car_running)) )
  )

(:action uber_to_busstop
    :parameters ()
    :precondition (and (need_taxi_to_busstop) (at_garrage))
    :effect (and (at_busstop) (need_to_find_bus) (not (at_garrage)) (not (need_taxi_to_busstop)))
  )

(:action check_time_at_busstop
    :parameters ()
    :precondition (and (at_busstop) (need_to_find_bus))
    :effect (and (not (need_to_find_bus))
                (probabilistic 1/2 (and (bus_on_time))
                   1/2 (and (bus_missed)))
    )
  )
  (:action take_bus_to_station_1
    :parameters ()
    :precondition (and (at_busstop) (bus_on_time))
    :effect (and (not (at_busstop))
                (not (bus_on_time))
                (at_station1)))

  (:action catch_train
    :parameters ()
    :precondition (and (at_station1) )
    :effect (probabilistic 1/2 (and (not (at_station1)) (on_board_train))
                           1/2 (and (missed_train))))

  (:action ride_train
    :parameters ()
    :precondition (and (on_board_train) )
    :effect (and (not (on_board_train))
                (probabilistic 1/2 (and (at_station3))
                           1/2 (and (at_station2)))
            ))


  (:action walk_to_airport
    :parameters ()
    :precondition (and (at_station3))
    :effect (and (at_airport) (at_north_gate) (not (at_station3)))
  )



  (:action take_taxi1
    :parameters ()
    :precondition (and (at_station1) (missed_train) )
    :effect  (and (at_airport) (not (at_station1)) (not (missed_train))))

  (:action take_taxi2
    :parameters ()
    :precondition (and (at_station2) )
    :effect  (and (at_airport) (at_south_gate) (not (at_station2))))

   (:action walk_to_north_gate
    :parameters ()
    :precondition (and (at_airport) (at_south_gate) )
    :effect  (and (at_north_gate) (not (at_south_gate)) )
   )
  (:action buy_ticket
    :parameters ()
    :precondition (and (at_airport) (at_north_gate))
    :effect (and (has_ticket) )
  )
  (:action go_to_gate_10
    :parameters ()
    :precondition (and (at_airport) (at_north_gate) (has_ticket))
    :effect (and (at_gate_10)  (not (at_north_gate)))
  )

  (:action board_flight
    :parameters ()
    :precondition (and (has_ticket) (at_gate_10))
    :effect (and (boarded_plane) ) (not (at_gate_10)))
)
