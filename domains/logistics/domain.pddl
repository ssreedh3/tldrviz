;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Go to your garrage
;; if your car is working take it to bus stop
;; else take a taxi
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain logistics)
  (:requirements )
  (:types )
  (:constants )
  (:predicates (truck1_at_stop1)
                (truck1_at_stop2)
                (truck1_at_stop3)
                (truck1_at_stop4)
                (truck1_at_stop5)
                (truck1_at_stop6)
                (package_at_stop2)
                (package_in_truck1)
                (package_in_barge)
                (truck1_at_port1)
                (barge_in_port1)
                (barge_in_port2)
                (barge_in_port3)
                (package_in_truck3)
                (package_in_truck2)
                (truck2_at_port3)
                (truck3_at_port4)
                (truck2_at_stop7)
                (truck3_at_stop8)
                (truck2_at_stop9)
                (truck3_at_stop9)
                (truck2_at_factory)
                (truck3_at_factory)
                (package_in_factory)
                 )

  (:action drive_truck1_from_stop1_to_stop20
    :parameters ()
    :precondition (and (truck1_at_stop1) )
    :effect (and (not (truck1_at_stop1)) (truck1_at_stop2)))

(:action pickup_package_from_stop2_truck10
    :parameters ()
    :precondition (and (truck1_at_stop2) (package_at_stop2))
    :effect (and  (not (package_at_stop2)) (package_in_truck1))
  )

(:action truck1_takes_road10
    :parameters ()
    :precondition (and (truck1_at_stop2))
    :effect (and (not (truck1_at_stop2)) (truck1_at_stop3))
    )


(:action truck1_takes_road11
    :parameters ()
    :precondition (and (truck1_at_stop2))
    :effect (and (not (truck1_at_stop2))
                    (truck1_at_stop4)
    )
  )

(:action drive_truck1_from_stop3_to_port10
    :parameters ()
    :precondition (and (truck1_at_stop3) )
    :effect (and (not (truck1_at_stop3)) (truck1_at_port1)))


  (:action drive_truck1_from_stop4_to_stop50
    :parameters ()
    :precondition (and (truck1_at_stop4) )
    :effect (and (not (truck1_at_stop4)) (truck1_at_stop5)))

(:action drive_truck1_from_stop5_to_stop60
    :parameters ()
    :precondition (and (truck1_at_stop5) )
    :effect (and (not (truck1_at_stop5)) (truck1_at_stop6)))

(:action drive_truck1_from_stop6_to_port10
    :parameters ()
    :precondition (and (truck1_at_stop6) )
    :effect (and (not (truck1_at_stop6)) (truck1_at_port1)))


(:action transfer_package_from_truck1_to_barge0
    :parameters ()
    :precondition (and (package_in_truck1) (barge_in_port1) (truck1_at_port1))
    :effect (and  (not (package_in_truck1)) (package_in_barge))
  )

  (:action barge_travels0
    :parameters ()
    :precondition (and (barge_in_port1) )
    :effect (and (not (barge_in_port1)(barge_in_port2))))


(:action barge_travels1
    :parameters ()
    :precondition (and (barge_in_port1) )
    :effect (and (not (barge_in_port1))
                           (barge_in_port3)))


  (:action transfer_package_from_barge_to_truck20
    :parameters ()
    :precondition (and (package_in_barge) (barge_in_port2) (truck2_at_port2))
    :effect (and  (not (package_in_barge)) (package_in_truck2))
  )


  (:action transfer_package_from_barge_to_truck30
    :parameters ()
    :precondition (and (package_in_barge) (barge_in_port3) (truck3_at_port3))
    :effect (and  (not (package_in_barge)) (package_in_truck3))
  )

    (:action drive_truck2_from_port2_to_stop70
    :parameters ()
    :precondition (and (truck2_at_port2) )
    :effect (and (not (truck2_at_port2)) (truck2_at_stop7)))

    (:action drive_truck3_from_port3_to_stop80
    :parameters ()
    :precondition (and (truck3_at_port3) )
    :effect (and (not (truck3_at_port3)) (truck3_at_stop8)))

    (:action drive_truck2_from_stop7_to_stop90
    :parameters ()
    :precondition (and (truck2_at_stop7) )
    :effect (and (not (truck2_at_stop7)) (truck2_at_stop9)))

    (:action drive_truck3_from_stop8_to_stop90
    :parameters ()
    :precondition (and (truck3_at_stop8) )
    :effect (and (not (truck3_at_stop8)) (truck3_at_stop9)))


    (:action drive_truck2_from_stop9_to_factory0
    :parameters ()
    :precondition (and (truck2_at_stop9) )
    :effect (and (not (truck2_at_stop9)) (truck2_at_factory)))

    (:action drive_truck3_from_stop9_to_factory0
    :parameters ()
    :precondition (and (truck3_at_stop9) )
    :effect (and (not (truck3_at_stop9)) (truck3_at_factory)))

    (:action truck2_deliver_to_factory0
    :parameters ()
    :precondition (and (truck2_at_factory) (package_in_truck2))
    :effect (and (not (package_in_truck2)) (package_in_factory)))

    (:action truck3_deliver_to_factory0
    :parameters ()
    :precondition (and (truck3_at_factory) (package_in_truck3))
    :effect (and (not (package_in_truck3)) (package_in_factory)))

)
