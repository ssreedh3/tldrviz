(define (problem p01)
  (:domain logistics)
  (:objects )
  (:init
  (truck1_at_stop1)
  (package_at_stop2)
  (barge_in_port1)
  (truck2_at_port2)
  (truck3_at_port3)
  )
  (:goal (and
  (package_in_factory)
  ))
)
