;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Go to your garrage
;; if your car is working take it to bus stop
;; else take a taxi
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain get_to_sf)
  (:requirements :probabilistic-effects :conditional-effects :negative-preconditions :equality :typing)
  (:types )
  (:constants )
  (:predicates (at_home)
                (uber_gift_card_1)
                (uber_gift_card_2)
                (used_up_uber_gift_card_1)
                (used_up_uber_gift_card_2)
                (at_garrage) (at_busstop)
                (car_running) (need_taxi_to_busstop)
                (at_busstop) (need_to_find_bus)
                (bus_on_time) (bus_missed)
                (at_station1) (at_station2) (at_station3) (at_airport) (on_board_train) (missed_train)
                (at_south_door)
                (at_north_door) (at_gate_10)
                (at_shuttle_stop1)
                (at_shuttle_stop2)
                (at_shuttle_stop3)
                (has_ticket) (boarded_plane)
                (tsa-pre-check-possible)
                (security-cleared)
                (need-to-use-general-security-channel)
                )

  (:action walk_to_garage
    :parameters ()
    :precondition (and (at_home) )
    :effect (and (at_garrage)  (not (at_home))
            (probabilistic 1/2 (and (car_running))
                           1/2 (and (need_taxi_to_busstop)))
            )
  )

(:action drive_to_busstop
    :parameters ()
    :precondition (and (car_running) (at_garrage))
    :effect (and (at_busstop) (need_to_find_bus) (not (at_garrage)) (not (car_running)) )
  )

(:action uber_to_busstop_with_gift_card_1
    :parameters ()
    :precondition (and (need_taxi_to_busstop) (at_garrage) (uber_gift_card_1))
    :effect (and (at_busstop) (need_to_find_bus) (used_up_uber_gift_card_1) (not (at_garrage)) (not (need_taxi_to_busstop)) (not (uber_gift_card_1)))
  )

(:action uber_to_busstop_with_gift_card_2
    :parameters ()
    :precondition (and (need_taxi_to_busstop) (at_garrage) (uber_gift_card_2))
    :effect (and (at_busstop) (need_to_find_bus) (used_up_uber_gift_card_2) (not (at_garrage)) (not (need_taxi_to_busstop)) (not (uber_gift_card_2)))
  )


(:action check_time_at_busstop
    :parameters ()
    :precondition (and (at_busstop) (need_to_find_bus))
    :effect (and (not (need_to_find_bus))
                (probabilistic 1/2 (and (bus_on_time))
                   1/2 (and (bus_missed)))
    )
  )

 (:action uber_to_station_1_with_gift_card_1
    :parameters ()
    :precondition (and (bus_missed) (at_busstop) (uber_gift_card_1))
    :effect (and (at_station1) (used_up_uber_gift_card_1) (not (at_busstop))  (not (uber_gift_card_1)))
  )

 (:action uber_to_station_1_with_gift_card_2
    :parameters ()
    :precondition (and (bus_missed) (at_busstop) (uber_gift_card_2))
    :effect (and (at_station1) (used_up_uber_gift_card_2) (not (at_busstop))  (not (uber_gift_card_2)))
  )

  (:action take_bus_to_station_1
    :parameters ()
    :precondition (and (at_busstop) (bus_on_time))
    :effect (and (not (at_busstop))
                (not (bus_on_time))
                (at_station1)))

  (:action catch_train
    :parameters ()
    :precondition (and (at_station1) )
    :effect (probabilistic 1/2 (and (not (at_station1)) (on_board_train))
                           1/2 (and (missed_train))))

  (:action ride_train
    :parameters ()
    :precondition (and (on_board_train) )
    :effect (and (not (on_board_train))
                (probabilistic 1/2 (and (at_station3))
                           1/2 (and (at_station2)))
            ))


  (:action walk_to_shuttle_stop_1
    :parameters ()
    :precondition (and (at_station1) (used_up_uber_gift_card_1) (used_up_uber_gift_card_2) (missed_train))
    :effect (and (not (at_station1)) (at_shuttle_stop1) (not (missed_train)))
  )

(:action walk_to_shuttle_stop_2
    :parameters ()
    :precondition (and (at_station2) (used_up_uber_gift_card_1) (used_up_uber_gift_card_2))
    :effect (and (not (at_station2)) (at_shuttle_stop2))
  )

  (:action take_shuttle_from_stop_1_to_stop_3
    :parameters ()
    :precondition (and (at_shuttle_stop1))
    :effect (and (not (at_shuttle_stop1)) (at_shuttle_stop3))
  )

(:action take_shuttle_from_stop_1_to_stop_3
    :parameters ()
    :precondition (and (at_shuttle_stop1))
    :effect (and (not (at_shuttle_stop1)) (at_shuttle_stop3))
  )

  (:action walk_to_airport_from_station_3
    :parameters ()
    :precondition (and (at_station3))
    :effect (and (at_airport) (at_north_door) (not (at_station3)))
  )

  (:action walk_to_airport_from_shuttle_stop_3
    :parameters ()
    :precondition (and (at_shuttle_stop3))
    :effect (and (at_airport) (at_north_door) (not (at_shuttle_stop3)))
  )



  (:action take_uber_to_airport_from_station_1_with_gift_card_1
    :parameters ()
    :precondition (and (at_station1) (missed_train) (uber_gift_card_1))
    :effect  (and (at_airport) (not (at_station1)) (not (missed_train)) (not (uber_gift_card_1))  (used_up_uber_gift_card_1)))

(:action take_uber_to_airport_from_station_1_with_gift_card_2
    :parameters ()
    :precondition (and (at_station1) (missed_train) (uber_gift_card_2))
    :effect  (and (at_airport) (not (at_station1)) (not (missed_train)) (not (uber_gift_card_2))  (used_up_uber_gift_card_2)))

  (:action take_uber_to_airport_from_station_2_with_gift_card_1
    :parameters ()
    :precondition (and (at_station2) (uber_gift_card_1))
    :effect  (and (at_airport) (at_south_door) (not (at_station2)) (not (uber_gift_card_1))  (used_up_uber_gift_card_1)))

(:action take_uber_to_airport_from_station_2_with_gift_card_2
    :parameters ()
    :precondition (and (at_station2) (uber_gift_card_2))
    :effect  (and (at_airport) (at_south_door) (not (at_station2)) (not (uber_gift_card_2)) (used_up_uber_gift_card_2)))






   (:action walk_to_north_door
    :parameters ()
    :precondition (and (at_airport) (at_south_door) )
    :effect  (and (at_north_door) (not (at_south_door)) )
   )
  (:action buy_ticket
    :parameters ()
    :precondition (and (at_airport) (at_north_door))
    :effect (and (has_ticket) )
  )




  (:action try_pre_checked_tsa
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (tsa-pre-check-possible))
    :effect (and
            (not (tsa-pre-check-possible))
            (probabilistic 1/2 (and (security-cleared))
                            1/2 (and (need-to-use-general-security-channel)))
    )
  )

  (:action try_general_security
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (need-to-use-general-security-channel))
    :effect (and
            (not (need-to-use-general-security-channel))
            (probabilistic 1/2 (and (security-cleared))
                           1/2 (and (need-to-do-patdown)))
    )
  )

  (:action gothrough_patdown
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (need-to-do-patdown))
    :effect (and
            (not (need-to-use-general-security-channel))
            (security-cleared)
            )
    )



  (:action go_to_gate_10
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (security-cleared))
    :effect (and (at_gate_10)  (not (at_north_door)))
  )

  (:action board_flight
    :parameters ()
    :precondition (and (has_ticket) (at_gate_10))
    :effect (and (boarded_plane)  (not (at_gate_10))))
)
