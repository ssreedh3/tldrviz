;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Go to your garrage
;; if your car is working take it to bus stop
;; else take a taxi
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain get_to_sf)
  (:requirements )
  (:types )
  (:constants )
  (:predicates (at_home)
                (uber_gift_card_1)
                (uber_gift_card_2)
                (used_up_uber_gift_card_1)
                (used_up_uber_gift_card_2)
                (at_garrage)
                (car_running) (need_taxi_to_busstop)
                (at_busstop) (need_to_find_bus)
                (bus_on_time) (bus_missed)
                (at_station1) (at_station2) (at_station3) (at_airport) (on_board_train) (missed_train)
                (at_south_door)
                (at_north_door) (at_gate_10)
                (at_shuttle_stop1)
                (at_shuttle_stop2)
                (at_shuttle_stop3)
                (has_ticket) (boarded_plane)
                (tsa-pre-check-possible)
                (security-cleared)
                (need-to-use-general-security-channel)
                (need-to-do-patdown)
                )

  (:action walk_to_garage0
    :parameters ()
    :precondition (and (at_home) )
    :effect (and (at_garrage)  (not (at_home)) (car_running))
  )

    (:action walk_to_garage1
    :parameters ()
    :precondition (and (at_home) )
    :effect (and (at_garrage)  (not (at_home)) (need_taxi_to_busstop)))

(:action drive_to_busstop0
    :parameters ()
    :precondition (and (car_running) (at_garrage))
    :effect (and (at_busstop) (need_to_find_bus) (not (at_garrage)) (not (car_running)) )
  )

(:action uber_to_busstop_with_gift_card_10
    :parameters ()
    :precondition (and (need_taxi_to_busstop) (at_garrage) (uber_gift_card_1))
    :effect (and (at_busstop) (need_to_find_bus) (used_up_uber_gift_card_1) (not (at_garrage)) (not (need_taxi_to_busstop)) (not (uber_gift_card_1)))
  )

(:action uber_to_busstop_with_gift_card_20
    :parameters ()
    :precondition (and (need_taxi_to_busstop) (at_garrage) (uber_gift_card_2))
    :effect (and (at_busstop) (need_to_find_bus) (used_up_uber_gift_card_2) (not (at_garrage)) (not (need_taxi_to_busstop)) (not (uber_gift_card_2)))
  )


(:action check_time_at_busstop0
    :parameters ()
    :precondition (and (at_busstop) (need_to_find_bus))
    :effect (and (not (need_to_find_bus)) (bus_on_time))

  )
(:action check_time_at_busstop1
    :parameters ()
    :precondition (and (at_busstop) (need_to_find_bus))
    :effect (and (not (need_to_find_bus)) (bus_missed)
    )
  )

 (:action uber_to_station_1_with_gift_card_10
    :parameters ()
    :precondition (and (bus_missed) (at_busstop) (uber_gift_card_1))
    :effect (and (at_station1) (used_up_uber_gift_card_1) (not (at_busstop))  (not (uber_gift_card_1)))
  )

 (:action uber_to_station_1_with_gift_card_20
    :parameters ()
    :precondition (and (bus_missed) (at_busstop) (uber_gift_card_2))
    :effect (and (at_station1) (used_up_uber_gift_card_2) (not (at_busstop))  (not (uber_gift_card_2)))
  )

  (:action take_bus_to_station_10
    :parameters ()
    :precondition (and (at_busstop) (bus_on_time))
    :effect (and (not (at_busstop))
                (not (bus_on_time))
                (at_station1)))

  (:action catch_train0
    :parameters ()
    :precondition (and (at_station1) )
    :effect (and (not (at_station1)) (on_board_train)))

  (:action catch_train1
    :parameters ()
    :precondition (and (at_station1) )
    :effect (and (missed_train))
    )

  (:action ride_train0
    :parameters ()
    :precondition (and (on_board_train) )
    :effect (and (not (on_board_train)) (at_station3))
            )

  (:action ride_train1
    :parameters ()
    :precondition (and (on_board_train) )
    :effect (and (not (on_board_train))
                (at_station2))
            )


  (:action walk_to_shuttle_stop_10
    :parameters ()
    :precondition (and (at_station1) (used_up_uber_gift_card_1) (used_up_uber_gift_card_2) (missed_train))
    :effect (and (not (at_station1)) (at_shuttle_stop1) (not (missed_train)))
  )

(:action walk_to_shuttle_stop_20
    :parameters ()
    :precondition (and (at_station2) (used_up_uber_gift_card_1) (used_up_uber_gift_card_2))
    :effect (and (not (at_station2)) (at_shuttle_stop2))
  )

  (:action take_shuttle_from_stop_1_to_stop_30
    :parameters ()
    :precondition (and (at_shuttle_stop1))
    :effect (and (not (at_shuttle_stop1)) (at_shuttle_stop3))
  )

(:action take_shuttle_from_stop_2_to_stop_30
    :parameters ()
    :precondition (and (at_shuttle_stop1))
    :effect (and (not (at_shuttle_stop1)) (at_shuttle_stop3))
  )

  (:action walk_to_airport_from_station_30
    :parameters ()
    :precondition (and (at_station3))
    :effect (and (at_airport) (at_north_door) (not (at_station3)))
  )

  (:action walk_to_airport_from_shuttle_stop_30
    :parameters ()
    :precondition (and (at_shuttle_stop3))
    :effect (and (at_airport) (at_north_door) (not (at_shuttle_stop3)))
  )



  (:action take_uber_to_airport_from_station_1_with_gift_card_10
    :parameters ()
    :precondition (and (at_station1) (missed_train) (uber_gift_card_1))
    :effect  (and (at_airport) (not (at_station1)) (not (missed_train)) (not (uber_gift_card_1))  (used_up_uber_gift_card_1)))

(:action take_uber_to_airport_from_station_1_with_gift_card_20
    :parameters ()
    :precondition (and (at_station1) (missed_train) (uber_gift_card_2))
    :effect  (and (at_airport) (not (at_station1)) (not (missed_train)) (not (uber_gift_card_2))  (used_up_uber_gift_card_2)))

  (:action take_uber_to_airport_from_station_2_with_gift_card_10
    :parameters ()
    :precondition (and (at_station2) (uber_gift_card_1))
    :effect  (and (at_airport) (at_south_door) (not (at_station2)) (not (uber_gift_card_1))  (used_up_uber_gift_card_1)))

(:action take_uber_to_airport_from_station_2_with_gift_card_20
    :parameters ()
    :precondition (and (at_station2) (uber_gift_card_2))
    :effect  (and (at_airport) (at_south_door) (not (at_station2)) (not (uber_gift_card_2)) (used_up_uber_gift_card_2)))




   (:action walk_to_north_door0
    :parameters ()
    :precondition (and (at_airport) (at_south_door) )
    :effect  (and (at_north_door) (not (at_south_door)) )
   )
  (:action buy_ticket0
    :parameters ()
    :precondition (and (at_airport) (at_north_door))
    :effect (and (has_ticket) )
  )

  (:action try_pre_checked_tsa0
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (tsa-pre-check-possible))
    :effect (and
            (not (tsa-pre-check-possible)) (security-cleared))
    )


  (:action try_pre_checked_tsa1
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (tsa-pre-check-possible))
    :effect (and
            (not (tsa-pre-check-possible)) (need-to-use-general-security-channel)))


  (:action try_general_security0
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (need-to-use-general-security-channel))
    :effect (and
            (not (need-to-use-general-security-channel)) (security-cleared))
  )

  (:action try_general_security1
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (need-to-use-general-security-channel))
    :effect (and
            (not (need-to-use-general-security-channel)) (need-to-do-patdown)))

  (:action gothrough_patdown0
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (need-to-do-patdown))
    :effect (and
            (not (need-to-use-general-security-channel))
            (security-cleared)
            )
    )

  (:action go_to_gate_100
    :parameters ()
    :precondition (and (at_airport) (at_north_door) (has_ticket) (security-cleared))
    :effect (and (at_gate_10)  (not (at_north_door)))
  )

  (:action board_flight0
    :parameters ()
    :precondition (and (has_ticket) (at_gate_10))
    :effect (and (boarded_plane)  (not (at_gate_10))))
)
