;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Go to your garrage
;; if your car is working take it to bus stop
;; else take a taxi
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain get_to_sf)
  (:requirements )
  (:types )
  (:predicates (avoid_proposition_at_home)
                (avoid_proposition_uber_gift_card_1)
                (avoid_proposition_uber_gift_card_2)
                (avoid_proposition_used_up_uber_gift_card_1)
                (avoid_proposition_used_up_uber_gift_card_2)
                (avoid_proposition_at_garrage)
                (avoid_proposition_car_running)
                (avoid_proposition_need_taxi_to_busstop)
                (avoid_proposition_at_busstop)
                (avoid_proposition_need_to_find_bus)
                (avoid_proposition_bus_on_time)
                (avoid_proposition_bus_missed)
                (avoid_proposition_at_station1)
                (avoid_proposition_at_station2)
                (avoid_proposition_at_station3)
                (avoid_proposition_at_airport)
                (avoid_proposition_on_board_train)
                (avoid_proposition_missed_train)
                (avoid_proposition_at_south_door)
                (avoid_proposition_at_north_door)
                (avoid_proposition_at_gate_10)
                (avoid_proposition_at_shuttle_stop1)
                (avoid_proposition_at_shuttle_stop2)
                (avoid_proposition_at_shuttle_stop3)
                (avoid_proposition_has_ticket)
                (avoid_proposition_boarded_plane)
                (avoid_proposition_tsa-pre-check-possible)
                (avoid_proposition_security-cleared)
                (avoid_proposition_need-to-use-general-security-channel)
                (avoid_proposition_need-to-do-patdown)
                 (at_home)
                (uber_gift_card_1)
                (uber_gift_card_2)
                (used_up_uber_gift_card_1)
                (used_up_uber_gift_card_2)
                (at_garrage)
                (car_running) (need_taxi_to_busstop)
                (at_busstop) (need_to_find_bus)
                (bus_on_time) (bus_missed)
                (at_station1) (at_station2) (at_station3) (at_airport) (on_board_train) (missed_train)
                (at_south_door)
                (at_north_door) (at_gate_10)
                (at_shuttle_stop1)
                (at_shuttle_stop2)
                (at_shuttle_stop3)
                (has_ticket) (boarded_plane)
                (tsa-pre-check-possible)
                (security-cleared)
                (need-to-use-general-security-channel)
                (need-to-do-patdown)
                 )


 {}
)
