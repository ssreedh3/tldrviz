;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Go to your garrage
;; if your car is working take it to bus stop
;; else take a taxi
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain logistics)
  (:requirements )
  (:types )
  (:constants )
  (:predicates (truck1_at_stop1)
                (truck1_at_stop2)
                (truck1_at_stop3)
                (truck1_at_stop4)
                (truck1_at_stop5)
                (truck1_at_stop6)
                (package_at_stop2)
                (package_in_truck1)
                (package_in_barge)
                (truck1_at_port1)
                (barge_in_port1)
                (barge_in_port2)
                (barge_in_port3)
                (package_in_truck3)
                (package_in_truck2)
                (truck2_at_port2)
                (truck3_at_port3)
                (truck2_at_stop7)
                (truck3_at_stop8)
                (truck2_at_stop9)
                (truck3_at_stop9)
                (truck2_at_factory)
                (truck3_at_factory)
                (package_in_factory)
                                (avoid_proposition_truck1_at_stop1)
                (avoid_proposition_truck1_at_stop2)
                (avoid_proposition_truck1_at_stop3)
                (avoid_proposition_truck1_at_stop4)
                (avoid_proposition_truck1_at_stop5)
                (avoid_proposition_truck1_at_stop6)
                (avoid_proposition_package_at_stop2)
                (avoid_proposition_package_in_truck1)
                (avoid_proposition_package_in_barge)
                (avoid_proposition_truck1_at_port1)
                (avoid_proposition_barge_in_port1)
                (avoid_proposition_barge_in_port2)
                (avoid_proposition_barge_in_port3)
                (avoid_proposition_package_in_truck3)
                (avoid_proposition_package_in_truck2)
                (avoid_proposition_truck2_at_port2)
                (avoid_proposition_truck3_at_port3)
                (avoid_proposition_truck2_at_stop7)
                (avoid_proposition_truck3_at_stop8)
                (avoid_proposition_truck2_at_stop9)
                (avoid_proposition_truck3_at_stop9)
                (avoid_proposition_truck2_at_factory)
                (avoid_proposition_truck3_at_factory)
                (avoid_proposition_package_in_factory)

                 )

    {}
)
