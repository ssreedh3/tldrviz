from flask import Flask, render_template, request, jsonify
import requests
import json
import yaml
import copy
from json import JSONEncoder, JSONDecoder
from policy_viewer_constants import *
from policy_viewer_globals import *
from utils import *

app = Flask(__name__)


# TODO: All of these should be in a databases
CURR_POLICY = []
CURR_POLICY_MAP = {}
CURR_LANDMARKS = {}
CURR_MODEL = {}
POLICY_GRAPH = {}
FACT_LIST = []
SELECTED_FACTS = []
CURR_DOMAIN_NAME = "get_sf_dummy"
START_STATE = None

# Undo control
UNDO_STACK = []

def push_to_the_stack():
    UNDO_STACK.append([CURR_POLICY, CURR_MODEL, CURR_LANDMARKS, POLICY_GRAPH, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME, START_STATE])

def undo_from_the_stack():
    global CURR_POLICY, POLICY_GRAPH, CURR_MODEL, CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME, START_STATE
    if len(UNDO_STACK) > 0:
        return UNDO_STACK.pop()
    else:
        return [CURR_POLICY, CURR_MODEL, CURR_LANDMARKS, POLICY_GRAPH, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME, START_STATE]
@app.route('/')
def get_index():
    global CURR_POLICY, POLICY_GRAPH, CURR_MODEL, CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME, START_STATE
    CURR_POLICY, CURR_MODEL, CURR_LANDMARKS, FACT_LIST = populate_original_global_dicts()
    SELECTED_FACTS = copy.deepcopy(FACT_LIST)

    POLICY_GRAPH, policy_json, new_policy, CURR_POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL, SELECTED_FACTS)
    START_STATE = copy.deepcopy(CURR_MODEL[INSTANCE][INIT])
    #policy_json = ''
    return render_template("index.html", curr_string=policy_json, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))

@app.route('/selected', methods=['GET'])
def get_selected():
    global CURR_POLICY, POLICY_GRAPH, CURR_MODEL, CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME, START_STATE
    curr_domain = request.args['selected_domain']
    CURR_POLICY, CURR_MODEL, CURR_LANDMARKS, FACT_LIST = populate_for_given_domain(curr_domain)
    SELECTED_FACTS = copy.deepcopy(FACT_LIST)
    CURR_DOMAIN_NAME = curr_domain
    POLICY_GRAPH, policy_json, new_policy, CURR_POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL, SELECTED_FACTS)
    START_STATE = copy.deepcopy(CURR_MODEL[INSTANCE][INIT])

    return render_template("index.html", curr_string=policy_json, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))


@app.route('/undo')
def perform_undo():
    global CURR_POLICY, POLICY_GRAPH, CURR_MODEL, CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME
    CURR_POLICY, CURR_MODEL, CURR_LANDMARKS, POLICY_GRAPH, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME, START_STATE = undo_from_the_stack()
    POLICY_GRAPH, policy_json, new_policy, CURR_POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL, SELECTED_FACTS)
    create_summary_and_graph(CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS)
    return render_template("index.html", curr_string=policy_json, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))



@app.route('/get_policy.json')
def get_policy():
    with open('./static/policy.json') as p_fd:
        data = json.load(p_fd)
    response = jsonify(data)
    return response

@app.route('/get_fact_list.json')
def get_fact_list():
    response = jsonify({'all_facts':FACT_LIST, 'selected_fact':SELECTED_FACTS})
    return response

@app.route('/get_summary.json')
def get_summary():
    with open('./static/summary.json') as p_fd:
        data = json.load(p_fd)
    response = jsonify(data)
    return response

@app.route('/get_successors.json')
def get_successors():
    with open('./static/successor.json') as p_fd:
        data = json.load(p_fd)
    response = jsonify(data)
    return response

@app.route('/filter_out')
def get_filtered():
    global CURR_POLICY, POLICY_GRAPH, CURR_MODEL, CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, START_STATE
    push_to_the_stack()
    SELECTED_FACTS = []
    for arg_key in request.args:
        if arg_key != 'Filter':
            SELECTED_FACTS.append(arg_key)
    #print ("Selected filter", SELECTED_FACTS)
    start_state_key = get_state_description(START_STATE)
    POLICY_GRAPH, policy_json, new_policy, CURR_POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL, SELECTED_FACTS, init_node=start_state_key)
    create_summary_and_graph(CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS)

    return render_template("index.html", curr_string=policy_json, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))

@app.route('/drilldown', methods=['GET'])
def drill_down():
    global POLICY_GRAPH, CURR_POLICY,CURR_MODEL, CURR_LANDMARKS, SELECTED_FACTS, CURR_POLICY_MAP, START_STATE
    new_landmark = [] # This should be generated by calling the rest api

    #print (POLICY_GRAPH)
    push_to_the_stack()
    start_state_fct = []
    start_state_fct.append(request.args['curr_fact'])
    for arg_key in request.args:
        if arg_key != 'curr_fact' and arg_key != 'goal':
            start_state_fct.append(arg_key)
    START_STATE = copy.deepcopy(start_state_fct)
    start_state = get_state_description(start_state_fct)

    #print ("Curr policy states", CURR_POLICY_MAP.keys())
    #for k in CURR_POLICY_MAP.keys():
    #    print ("Current states>>>", k, start_state)
    if start_state not in CURR_POLICY_MAP:
        #print ("State missing", start_state, CURR_POLICY_MAP.keys())
        POLICY_GRAPH, policy_json, new_policy, POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL, SELECTED_FACTS)
        return render_template("index.html", curr_string=policy_json, error_msg="The selected state does not exist")

    else:
        goal_state = request.args['goal']
        # TODO: Make new version of the model
        new_model = copy.deepcopy(CURR_MODEL)
        new_model[INSTANCE][INIT] = set(start_state_fct)
        new_model[INSTANCE][GOAL] = set([goal_state])
        POLICY_GRAPH, policy_json, CURR_POLICY, CURR_POLICY_MAP = get_policy_graph_fragment(start_state, goal_state, CURR_POLICY,CURR_MODEL, POLICY_GRAPH, SELECTED_FACTS)
        r = requests.get(SUBGOAL_GEN_URL, {POLICY_KEY:yaml.dump(CURR_POLICY), DET_MODEL_KEY: yaml.dump(new_model)})

        CURR_LANDMARKS = yaml.load(r.text)
        #print ("### CURR LANDM", CURR_LANDMARKS)

        for node in CURR_LANDMARKS:
            if len(CURR_LANDMARKS[node]) == 0:
                CURR_LANDMARKS[node].add(start_state)
        CURR_LANDMARKS[start_state] = set()
        create_summary_and_graph(CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS)

        return render_template("index.html", curr_string=policy_json, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))

@app.route('/query')
def query():
    global CURR_POLICY, POLICY_GRAPH, CURR_MODEL, CURR_LANDMARKS, FACT_LIST, SELECTED_FACTS, CURR_POLICY_MAP, CURR_DOMAIN_NAME
    push_to_the_stack()
    fact_to_be_avoided = request.args['subgoal']
    #print (">>Domain name", CURR_DOMAIN_NAME)

    status = test_unsolvability(CURR_MODEL, fact_to_be_avoided, CURR_DOMAIN_NAME)
    POLICY_GRAPH, policy_json, new_policy, CURR_POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL,
                                                                                 SELECTED_FACTS)

    if status:
        return render_template("index.html", curr_string=policy_json, error_msg="The goal can not be reached without achieving the subgoal "+fact_to_be_avoided, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))
    else:
        return render_template("index.html", curr_string=policy_json, domain_name=CURR_DOMAIN_NAME, init=" ".join(CURR_MODEL[INSTANCE][INIT]), goal=" ".join(CURR_MODEL[INSTANCE][GOAL]))


if __name__ == "__main__":
    app.run(host='0.0.0.0')