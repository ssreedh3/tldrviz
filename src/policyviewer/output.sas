begin_version
3
end_version
begin_metric
0
end_metric
7
begin_variable
var0
-1
4
Atom truck3_at_factory()
Atom truck3_at_port3()
Atom truck3_at_stop8()
Atom truck3_at_stop9()
end_variable
begin_variable
var1
-1
4
Atom truck2_at_factory()
Atom truck2_at_port2()
Atom truck2_at_stop7()
Atom truck2_at_stop9()
end_variable
begin_variable
var2
-1
7
Atom truck1_at_port1()
Atom truck1_at_stop1()
Atom truck1_at_stop2()
Atom truck1_at_stop3()
Atom truck1_at_stop4()
Atom truck1_at_stop5()
Atom truck1_at_stop6()
end_variable
begin_variable
var3
-1
3
Atom barge_in_port1()
Atom barge_in_port3()
<none of those>
end_variable
begin_variable
var4
-1
2
Atom barge_in_port2()
NegatedAtom barge_in_port2()
end_variable
begin_variable
var5
-1
6
Atom package_at_stop2()
Atom package_in_barge()
Atom package_in_factory()
Atom package_in_truck1()
Atom package_in_truck2()
Atom package_in_truck3()
end_variable
begin_variable
var6
-1
2
Atom avoid_proposition_truck1_at_port1()
NegatedAtom avoid_proposition_truck1_at_port1()
end_variable
1
begin_mutex_group
2
3 0
4 0
end_mutex_group
begin_state
1
1
1
0
1
0
0
end_state
begin_goal
2
5 2
6 0
end_goal
22
begin_operator
barge_travels0 
0
2
0 3 0 2
0 4 -1 0
1
end_operator
begin_operator
barge_travels1 
0
1
0 3 0 1
1
end_operator
begin_operator
drive_truck1_from_stop1_to_stop20 
0
1
0 2 1 2
1
end_operator
begin_operator
drive_truck1_from_stop3_to_port10 
0
2
0 6 -1 1
0 2 3 0
1
end_operator
begin_operator
drive_truck1_from_stop4_to_stop50 
0
1
0 2 4 5
1
end_operator
begin_operator
drive_truck1_from_stop5_to_stop60 
0
1
0 2 5 6
1
end_operator
begin_operator
drive_truck1_from_stop6_to_port10 
0
2
0 6 -1 1
0 2 6 0
1
end_operator
begin_operator
drive_truck2_from_port2_to_stop70 
0
1
0 1 1 2
1
end_operator
begin_operator
drive_truck2_from_stop7_to_stop90 
0
1
0 1 2 3
1
end_operator
begin_operator
drive_truck2_from_stop9_to_factory0 
0
1
0 1 3 0
1
end_operator
begin_operator
drive_truck3_from_port3_to_stop80 
0
1
0 0 1 2
1
end_operator
begin_operator
drive_truck3_from_stop8_to_stop90 
0
1
0 0 2 3
1
end_operator
begin_operator
drive_truck3_from_stop9_to_factory0 
0
1
0 0 3 0
1
end_operator
begin_operator
pickup_package_from_stop2_truck10 
1
2 2
1
0 5 0 3
1
end_operator
begin_operator
transfer_package_from_barge_to_truck20 
2
4 0
1 1
1
0 5 1 4
1
end_operator
begin_operator
transfer_package_from_barge_to_truck30 
2
3 1
0 1
1
0 5 1 5
1
end_operator
begin_operator
transfer_package_from_truck1_to_barge0 
2
3 0
2 0
1
0 5 3 1
1
end_operator
begin_operator
transfer_package_from_truck1_to_barge_without_reaching_port0 
2
3 0
2 6
1
0 5 3 1
1
end_operator
begin_operator
truck1_takes_road10 
0
1
0 2 2 3
1
end_operator
begin_operator
truck1_takes_road11 
0
1
0 2 2 4
1
end_operator
begin_operator
truck2_deliver_to_factory0 
1
1 0
1
0 5 4 2
1
end_operator
begin_operator
truck3_deliver_to_factory0 
1
0 0
1
0 5 5 2
1
end_operator
0
