import os

SUBGOAL_GEN_URL = "http://127.0.0.1:8080/subgoals_from_map"
POLICY_KEY = "policy"
DOMAIN_KEY = "det_domain"
PROBLEM_KEY = "det_problem"
DET_MODEL_KEY = 'det_model_map'

ACT = "act"
PARARMETERS = "params"
ADDS = "adds"
DELS = "dels"
POS_PREC = "pos"
COND_ADDS = "COND_ADDS"
COND_DELS = "COND_DELS"
DOMAIN = "domain"
INSTANCE = "instance"
INIT = "init"
GOAL_ACT = "goal-act"
GROUND_ACT_TEMPL = "(:action {} \n:parameters () \n :precondition (and {} )\n :effect (and {} ))"


ACT_TO_DET_MAP = "map_key"
DET_ACT_DIVIDER = "#"
GOAL = "goal"
DONE = "done"
NODES = "nodes"
LINKS = "links"
DESCRIPTION = "description"
TARGET = "target"
SOURCE = "source"
ALL_FACTS = "ALL_FACTS"
TYPE = "type"
SUCCESSORS = "successors"
STATE_SEPARATOR = ","

AVOID_PROPOSITION = "avoid_proposition_"

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_FOLDER = os.path.join(CURRENT_DIR, "DATA")
#DATA_FOLDER = "/home/local/ASUAD/ssreedh3/mycode/DATAVIZ_PROJ/MDP-Policy-Transparency-Sarath/src/policyviewer/DATA/"
ORIGINAL_POLICY_GRAPH_FILE = os.path.join(DATA_FOLDER, "policy.json")
ORIGINAL_LANDMARK_FILE = os.path.join(DATA_FOLDER, "summary.json")
ORIGINAL_SUCC_FILE = os.path.join(DATA_FOLDER, "successor.json")

ORIGINAL_DOMAIN_FILE = os.path.join(DATA_FOLDER, "domain.pddl")
ORIGINAL_PROBLEM_FILE = os.path.join(DATA_FOLDER, "problem.pddl")
RAW_POLICY_FILE = os.path.join(DATA_FOLDER, "curr_policy")



PLANNING_CMD = "./run_planner.sh {} {}"
