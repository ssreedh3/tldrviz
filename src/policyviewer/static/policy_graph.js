var zoom_flag = false;
var width = 1000;
var height = 1000;
var margin = 100;
var svg = d3.select(".policy");
d3.select(".policy").attr("width", width).attr("height",height).attr("viewBox", [0, 0, width, height]);

var legend_svg = d3.select(".legends");
legend_svg.attr("width", 200).attr("height",100);

legend_svg.append('circle').attr('fill', 'black').attr('r',10).attr('cx',20).attr('cy',20);
legend_svg.append('text').style('text-anchor','left').attr('fill', 'black').text('Initial state').attr('x',35).attr('y',25);
legend_svg.append('circle').style('text-anchor','left').attr('fill', 'grey').attr('r',10).attr('cx',20).attr('cy',45);
legend_svg.append('text').attr('fill', 'black').text('Intermediate state').attr('x',35).attr('y',50);
legend_svg.append('circle').style('text-anchor','left').attr('fill', 'green').attr('r',10).attr('cx',20).attr('cy',70);
legend_svg.append('text').attr('fill', 'green').text('Goal state').attr('x',35).attr('y',75);


var source_selected = false;
var curr_source = undefined;
var destination_selected = false;
var curr_destination = undefined;
var selected_query_fact = undefined;



//var zm =


//zm(svg);

function visualize_graph(data, is_summary = false){
    // Assume python has already created the csv with the unique nodes
    svg.selectAll('g').remove();
    svg.selectAll('circle').remove();

    var graph_parent = svg.append('g');
    var it_id;
    graph_parent.call(d3.zoom().scaleExtent([0.3, 1]).on('zoom', zoom_transform));

    function zoom_transform(){
    if (zoom_flag == true){
    graph_parent.attr('transform', d3.event.transform);
    }

}


    var policy_simul = d3.forceSimulation().nodes(data.nodes)
        .force("links", d3.forceLink(data.links))
        .force("charge", d3.forceManyBody().strength(-50))
        .force("center", d3.forceCenter(500,500));

    link_set = graph_parent.append('g').selectAll('line').data(data.links).enter()
        .append('line').style("stroke","black").style("stroke-width",2).on("mouseover", function (d) {
            // svg.selectAll(".hovertext").remove();
            // if (is_summary == true){
            //     svg.append("text")
            //         .attr("class", "hovertext")
            //         .attr("x", d3.event.pageX)
            //         .attr("y", d3.event.pageY  - 150)
            //         .attr("fill","black")
            //         .text(d.description);
            // }else{
                d3.select(".action-info").selectAll("p").remove();
                //d3.select(".action-info").append("p").style("color", "white").text(d.act.replace(/,/g," "));
            d3.select(".action-info").append("p").style("color", "black").text(d.act.replace(/,/g,", "));
            //}

        }).on("mouseout", function (d) {
            //svg.selectAll(".hovertext").remove();
            d3.select(".action-info").selectAll("p").remove();
        });

    var drag_element = d3.drag()
    .on("start", function(d){
        policy_simul.alphaTarget(0.8).restart();
        d.fx = d.x;d.fy = d.y;})
    .on("drag", function(d){ d.fx = d3.event.x;d.fy = d3.event.x;})
    .on("end", function(d){
          policy_simul.alphaTarget(0);
          if (d.type == "init"){
            d.fx = 100;
            d.fy = 500;
        }else{
              d.fx = null;d.fy = null;
          }
        });

    node_set = graph_parent.append('g').selectAll('circle').data(data.nodes).enter()
        .append('circle').attr('r', '10').attr("fill",function (d) {
            if (d.type == "goal"){
                return "green";
            }else if (d.type == "init"){
                return "black";
            }else{
                return "grey";
            }

    }).on("click", function (d) {
        if (is_summary == true){
            selected_query_fact = d.description;
            setup_query_panel();
            // Clicking here should initiate the drill down procedure
            if (source_selected == false){
                source_selected = true;
                curr_source =  d.description;
                setup_drill_down_start();
            }else if(destination_selected == false){
                destination_selected = true;
                curr_destination = d.description;
                setup_drill_down_end();
            }
            else{
                alert("Click on reset to unselect current selections")
                curr_source = undefined;
                curr_destination = undefined;

            }
        }

        }).on("mouseover", function (d) {
            // svg.selectAll(".hovertext").remove();
            // if (is_summary == true){
            //     svg.append("text")
            //         .attr("class", "hovertext")
            //         .attr("x", d3.event.pageX)
            //         .attr("y", d3.event.pageY  - 150)
            //         .attr("fill","black")
            //         .text(d.description);
            // }else{
                d3.select(".state-info").selectAll("p").remove();
                //d3.select(".state-info").append("p").style("color", "white").text(d.description.replace(/,/g," "));
            d3.select(".state-info").append("p").style("color", "black").text(d.description.replace(/,/g,", "));
            //}

        }).on("mouseout", function (d) {
            //svg.selectAll(".hovertext").remove();
            d3.select(".state-info").selectAll("p").remove();
        }).call(drag_element);
    for (it_id=0; it_id < data.nodes.length; it_id++){
        if (data.nodes[it_id].type == "init"){
            data.nodes[it_id].fx = 100;
            data.nodes[it_id].fy = 500;
        }

    }


    policy_simul.on("tick", function () {
        link_set.attr("x1", function (d) {

            return d.source.x;

        }).attr("y1",
                function (d) {
            return d.source.y;

        }).attr("x2", function (d) {
            return d.target.x;

        }).attr("y2",
                function (d) {
                    return d.target.y;
                });
        node_set
            .attr("cx", function (d) {
                //alert(d.y);
                return d.x;
            }).attr("cy",
                function (d) {
                    return d.y;
            });
    });

}


function visualize_policy_from_url(){
    policy = JSON.parse(curr_string.replace(/&quot;/ig,'"'));
    alert(policy);
    //d3.json("./static/policy.json", function (policy) {
    visualize_graph(policy);
    //});
}

function visualize_policy_from_url(){
    var current_view = d3.select("#CURRENTVIEW");
    current_view.selectAll('h5').remove();
    current_view.append('h5').text('Complete view');
 d3.json("./get_policy.json", function (policy) {
        visualize_graph(policy);
    });
}

function visualize_policy(){
    var current_view = d3.select("#CURRENTVIEW");
    current_view.selectAll('h5').remove();
    current_view.append('h5').text('Complete view');
    d3.json("./static/policy.json", function (policy) {
        visualize_graph(policy);
    });
}

function visualize_summary(){
    var current_view = d3.select("#CURRENTVIEW");
    current_view.selectAll('h5').remove();
    current_view.append('h5').text('Subgoals to be achieved');
    d3.json("./get_summary.json", function (summ) {
        visualize_graph(summ, true);
    });
}
//TODO:
var succ_map = undefined;
d3.json('./get_successors.json', function (data) {
    succ_map = data;

});
//alert(succ_map["ALL_FACTS"]);
function setup_drill_down_start(){
    var fact_list = d3.select("#START_STATE_LIST");

    var ALL_FACTS = succ_map["ALL_FACTS"];
    var facts_to_be_skipped = [];

    if (source_selected == true){
        var facts_to_be_skipped = succ_map[curr_source];

        var lists_to_be_shown = [];
        for (let i=0; i < ALL_FACTS.length; i++){

            if (facts_to_be_skipped.indexOf(ALL_FACTS[i]) == -1 && ALL_FACTS[i]!= curr_source){
                lists_to_be_shown.push(ALL_FACTS[i]);
            }
        }
        lists_to_be_shown.sort();
        fact_list.append("p").text(function () {
            return "Selected fact: "+ curr_source;
        });
        fact_list.append("input").attr("type", "hidden")
        .attr("name","curr_fact").attr("value", function () {
        return curr_source;
    });
        new_div = fact_list
            .selectAll("div").data(lists_to_be_shown).enter().append("div");
        new_div.append("input").attr("type","checkbox").attr("name", function (d) {
          return d;
        }).attr("value", function (d) {
                return d;
            });

        new_div.append("text").text(function (d) {
                return d;
            });
        new_div.append("br");

    }

}

function setup_drill_down_end(){
    var fact_list = d3.select("#END_STATE_FACT");

    var new_div = fact_list.append('div');
    new_div.append("text").text(function () {
        return "Goal fact: "  + curr_destination;

    });
    new_div.append("input").attr("type", "hidden")
        .attr("name","goal").attr("value", function () {
        return curr_destination;
    });

}

function setup_query_panel(){
    var fact_list = d3.select("#SELECTED_SUBGOAL");

    var new_div = fact_list.append('div');
    new_div.append("text").text(function () {
        return "Slected fact: "  + selected_query_fact;

    });

    new_div.append("input").attr("type", "hidden")
        .attr("name","subgoal").attr("value", function () {
        return selected_query_fact;
    });
}


function setup_filter_list() {
    var filter_list = d3.select("#FILTER_LIST");

    d3.json("./get_fact_list.json", function (fc_list) {
        var fact_list = fc_list['all_facts']
        fact_list.sort();
        var filter_div = filter_list.selectAll('input').data(fact_list).enter().append('div');

        filter_div.append('input').attr("type","checkbox").attr("name", function (d) {
            return d;
        }).attr("value", function (d) {
            return d;
        }).property('checked',function (d) {
            if (fc_list['selected_fact'].indexOf(d) != -1){
                return true;
            }else{
                return false;
            }
        });
        filter_div.append('text').text(function (d) {
            return d;
        });
        filter_div.append('br');
    });
}

visualize_policy_from_url();

setup_filter_list();