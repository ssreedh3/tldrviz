from queue import Queue
import os
import requests
import json
import copy
from policy_viewer_constants import *
import tarski
import tarski.io
from tarski.io.fstrips import print_init, print_goal, print_formula, print_atom
from tarski.syntax import CompoundFormula, formulas, Tautology
from tarski.fstrips import AddEffect, DelEffect
from psummarizer.utils import parse_policy
import tempfile
import yaml


# TODO: Allow for undo
def populate_original_global_dicts():
    CURR_POLICY = parse_policy(RAW_POLICY_FILE)
    POLICY_GRAPH = {}
    curr_facts = set()

    MODEL_DICT = parse_model(ORIGINAL_DOMAIN_FILE, ORIGINAL_PROBLEM_FILE)
    with open(ORIGINAL_POLICY_GRAPH_FILE) as p_fd:
        policy_dict = json.load(p_fd)
    #node_list = []
    for node in policy_dict[NODES]:
       for fct in node[DESCRIPTION].split(','):
           curr_facts.add(fct)
    #    POLICY_GRAPH[node[DESCRIPTION]] = {TYPE: node[TYPE], SUCCESSORS: set()}

    #for edges in policy_dict[LINKS]:
    #    POLICY_GRAPH[node_list[edges[SOURCE]]][SUCCESSORS].add(node_list[edges[TARGET]])
    #POLICY_GRAPH, policy_json, new_policy = create_policy_graph(CURR_POLICY, MODEL_DICT)

    CURR_LANDMARK = {}
    with open(ORIGINAL_LANDMARK_FILE) as l_fd:
        landmark_dict = json.load(l_fd)

    landmark_list = []
    for node in landmark_dict[NODES]:
        landmark_list.append(node[DESCRIPTION])
        CURR_LANDMARK[node[DESCRIPTION]] = set()

    for edges in landmark_dict[LINKS]:
        CURR_LANDMARK[landmark_list[edges[TARGET]]].add(landmark_list[edges[SOURCE]])


    print ("curr landmarek",CURR_LANDMARK)
    create_summary_and_graph(CURR_LANDMARK, list(curr_facts))

    return CURR_POLICY, MODEL_DICT, CURR_LANDMARK, list(curr_facts)



def populate_for_given_domain(domain_name):

    DOMAIN_DIR = os.path.join(DATA_FOLDER, domain_name)
    DOMAIN_FILE = os.path.join(DOMAIN_DIR, 'domain_det.pddl')
    PROBLEM_FILE = os.path.join(DOMAIN_DIR, 'problem.pddl')
    POLICY_FILE = os.path.join(DOMAIN_DIR, 'policy_file')

    CURR_POLICY = parse_policy(POLICY_FILE)

    print (">>>CURR POLIXY", CURR_POLICY)


    curr_facts = set()

    MODEL_DICT = parse_model(DOMAIN_FILE, PROBLEM_FILE)

    for st, act in CURR_POLICY:
        for fct in st:
            curr_facts.add(fct)


    start_state = get_state_description(MODEL_DICT[INSTANCE][INIT])

    r = requests.get(SUBGOAL_GEN_URL, {POLICY_KEY: yaml.dump(CURR_POLICY), DET_MODEL_KEY: yaml.dump(MODEL_DICT)})

    CURR_LANDMARKS = yaml.load(r.text)
    print("### CURR LANDM", CURR_LANDMARKS)

    for node in CURR_LANDMARKS:
        if len(CURR_LANDMARKS[node]) == 0:
            CURR_LANDMARKS[node].add(start_state)
    CURR_LANDMARKS[start_state] = set()


    print ("curr landmarek",CURR_LANDMARKS)
    create_summary_and_graph(CURR_LANDMARKS, list(curr_facts))

    return CURR_POLICY, MODEL_DICT, CURR_LANDMARKS, list(curr_facts)

def remove_parenthesis_from_name(full_name):
    '''
    For action and predicate names
    '''
    # TODO: Figure out why tarski is producing double ??
    prop_name = full_name.strip().replace('(', '').replace(')', '').replace('??', '?').strip()

    return prop_name


def parse_model(domain_file, problem_file):
    act_det_map = {}
    # Assume the problem and domain is fully grounded
    # Read the pddl files
    curr_reader = tarski.io.FstripsReader()
    curr_reader.read_problem(domain_file, problem_file)

    # Make the sets for init and goal
    init_set = set([remove_parenthesis_from_name(fact) for fact in print_init(curr_reader.problem).split("\n")])
    assert isinstance(curr_reader.problem.goal, CompoundFormula) or isinstance(curr_reader.problem.goal, formulas.Atom)
    if isinstance(curr_reader.problem.goal, CompoundFormula):
        goal_set = set([remove_parenthesis_from_name(print_formula(fact))
                        for fact in curr_reader.problem.goal.subformulas])
    else:
        goal_set = set([remove_parenthesis_from_name(print_formula(curr_reader.problem.goal))])
    # Make the dictionary for actions
    action_model = {}
    for act in curr_reader.problem.actions.values():
        #print(">>>>>>>>>>>>>>>>>>>>>>>>>action names", act)

        #if DET_ACT_DIVIDER in act.name:
        orig_act_name = act.name[:-1]
        if orig_act_name not in act_det_map:
            act_det_map[orig_act_name] = []
        if act.name not in act_det_map[orig_act_name]:
            act_det_map[orig_act_name].append(act.name)


        action_model[act.name] = {}
        # Add parameter list
        action_model[act.name][PARARMETERS] = [p.symbol for p in act.parameters]

        # Make sure the precondition is just a simple conjunction of positive literals
        assert isinstance(act.precondition, CompoundFormula) or isinstance(act.precondition, formulas.Atom)
        if isinstance(act.precondition, CompoundFormula):
            action_model[act.name][POS_PREC] = set([remove_parenthesis_from_name(print_formula(f))
                                                          for f in act.precondition.subformulas])
        else:
             action_model[act.name][POS_PREC] = set([remove_parenthesis_from_name(print_atom(act.precondition))])
        # Parse effects
        action_model[act.name][ADDS] = set()
        action_model[act.name][DELS] = set()
        action_model[act.name][COND_ADDS] = []
        action_model[act.name][COND_DELS] = []
        for curr_effs in act.effects:
            if type(curr_effs) != list:
                curr_effs = [curr_effs]
            for eff in curr_effs:
                # Todo: For some reason tarski is generating None effects
                if eff:
                    conditional = not isinstance(eff.condition, Tautology)
                    if conditional:
                        # Conditional effects should be of the form [[condition,eff]]
                        curr_condition = []
                        assert isinstance(eff.condition, CompoundFormula) or isinstance(eff.condition,
                                                                                               formulas.Atom)
                        if isinstance(eff.condition, CompoundFormula):
                            for f in eff.condition.subformulas:
                                curr_condition.append(remove_parenthesis_from_name(print_formula(f)))
                        else:
                            curr_condition.append(remove_parenthesis_from_name(print_atom(eff.condition)))

                        if isinstance(eff, AddEffect):
                            action_model[act.name][COND_ADDS].append([curr_condition, remove_parenthesis_from_name(print_atom(eff.atom))])
                        elif isinstance(eff, DelEffect):
                            action_model[act.name][COND_DELS].append([curr_condition, remove_parenthesis_from_name(print_atom(eff.atom))])
                    else:
                        if isinstance(eff, AddEffect):
                            action_model[act.name][ADDS].add(remove_parenthesis_from_name(print_atom(eff.atom)))
                        elif isinstance(eff, DelEffect):
                            action_model[act.name][DELS].add(remove_parenthesis_from_name(print_atom(eff.atom)))
    model_dict = {}
    model_dict[DOMAIN] = action_model
    model_dict[ACT_TO_DET_MAP] = act_det_map
    model_dict[INSTANCE] = {}
    model_dict[INSTANCE][INIT] = init_set
    model_dict[INSTANCE][GOAL] = goal_set
    return model_dict

def get_pos_prop_name(prop):
    return '('+prop+')'

def get_neg_prop_name(prop):
    return '(not ('+prop+'))'

def write_det_model(model_dict, domain_name, dst_domain_file, dst_prob_file):
    DOMAIN_DIR = os.path.join(DATA_FOLDER, domain_name)
    DOMAIN_TEMPL_FILE = os.path.join(DOMAIN_DIR, 'domain_templ.pddl')
    PROBLEM_TEMPL_FILE = os.path.join(DOMAIN_DIR, 'problem_templ.pddl')
    init_str = "\n".join([get_pos_prop_name(pr) for pr in model_dict[INSTANCE][INIT]])
    goal_str = "\n".join([get_pos_prop_name(pr) for pr in model_dict[INSTANCE][GOAL]])
    act_str_list = []

    for act in model_dict[DOMAIN]:
        act_str_list.append(GROUND_ACT_TEMPL.format(act,
            "\n".join([get_pos_prop_name(pr) for pr in model_dict[DOMAIN][act][POS_PREC]]),
        "\n".join([get_pos_prop_name(pr) for pr in model_dict[DOMAIN][act][ADDS]]) +
        "\n".join([get_neg_prop_name(pr) for pr in model_dict[DOMAIN][act][DELS]])
        ))
    act_str = "\n".join(act_str_list)

    with open(DOMAIN_TEMPL_FILE) as d_fd:
        domain_str = d_fd.read()

    with open(PROBLEM_TEMPL_FILE) as d_fd:
        prob_str = d_fd.read()

    with open(dst_domain_file,'w') as d_fd:
        d_fd.write(domain_str.format(act_str))

    with open(dst_prob_file,'w') as d_fd:
        d_fd.write(prob_str.format(init_str, goal_str))

def get_state_description(st, SELECTED_FACTS = []):
    if len(SELECTED_FACTS) > 0:
        filtered_list = list(set(st) & set(SELECTED_FACTS))
        return STATE_SEPARATOR.join(sorted(filtered_list))
    else:
        return STATE_SEPARATOR.join(sorted(st))

def create_policy_graph(CURR_POLICY,CURR_MODEL, SELECTED_FACTS=[], subset_nodes = set(), init_node = None, goal_fact=None):
    if len(subset_nodes) > 0:
        FULL_POLICY = False
    else:
        FULL_POLICY = True
    assert len(CURR_POLICY) > 0 and len(CURR_MODEL[DOMAIN]) > 0, "Empty policy or empty model"
    nodes = []
    dead_end_nodes = set()
    goal_nodes = set()
    succ_map = {}
    POLICY_GRAPH = {}
    POLICY_MAP = {}

    #print (">> Det map>>",CURR_MODEL[ACT_TO_DET_MAP])
    new_policy = []

    for st, st_act in CURR_POLICY:
        #print (">>> St", st)
        st_key = get_state_description(st, SELECTED_FACTS)
        st_set = set(st)
        POLICY_MAP[st_key] = st_act
        if FULL_POLICY or st_key in subset_nodes:
            new_act = st_act
            if st_key not in nodes:
                nodes.append(st_key)

            if st_act == GOAL_ACT:
                goal_nodes.add(st_key)

            elif goal_fact and goal_fact in st_key:
                goal_nodes.add(st_key)
                new_act = GOAL_ACT
            elif st_act == DONE:
                dead_end_nodes.add(st_key)
            else:
                if st_key not in succ_map:
                    succ_map[st_key] = set()
                for act in CURR_MODEL[ACT_TO_DET_MAP][st_act]:
                    curr_adds = set()
                    curr_dels = set()
                    curr_adds |= CURR_MODEL[DOMAIN][act][ADDS]
                    curr_dels |= CURR_MODEL[DOMAIN][act][DELS]
                    for c, e in CURR_MODEL[DOMAIN][act][COND_ADDS]:
                        if c <= st_set:
                            curr_adds.add(e)
                    for c, e in CURR_MODEL[DOMAIN][act][COND_DELS]:
                        if c <= st_set:
                            curr_dels.add(e)
                    succ_state = list(st_set - curr_dels | curr_adds)
                    succ_state_key = get_state_description(succ_state, SELECTED_FACTS)
                    #print(">>> St succ state", succ_state_key)

                    if FULL_POLICY or succ_state_key in subset_nodes:
                        succ_map[st_key].add(succ_state_key)
            new_policy.append([st, new_act])

    policy_graph = {NODES:[], LINKS:[]}


    if init_node:
        init_key = get_state_description(init_node.split(STATE_SEPARATOR), SELECTED_FACTS)
    else:
        init_key = get_state_description(list(CURR_MODEL[INSTANCE][INIT]), SELECTED_FACTS)
    for st_key in nodes:
        state_type = "default"
        if st_key == init_key:
            state_type = "init"
        elif st_key in goal_nodes:
            state_type = "goal"
        elif st_key in dead_end_nodes:
            state_type = "deadend"
        policy_graph[NODES].append({DESCRIPTION:st_key, TYPE:state_type})

        POLICY_GRAPH[st_key] = {}
        POLICY_GRAPH[st_key][TYPE] = state_type
        POLICY_GRAPH[st_key][SUCCESSORS] = set()
    #print ("SUCC MAP >>>>",succ_map)
    for sc_ind in range(len(policy_graph[NODES])):
        source_node = policy_graph[NODES][sc_ind][DESCRIPTION]
        if source_node in succ_map:
            for succ_node in succ_map[source_node]:
                try:
                    target_ind = nodes.index(succ_node)
                    policy_graph[LINKS].append({SOURCE: sc_ind,TARGET: target_ind, ACT: POLICY_MAP[source_node]})
                    POLICY_GRAPH[source_node][SUCCESSORS].add(succ_node)
                except IndexError:
                    print ("Index error....")
                    pass

    with open('./static/policy.json', 'w') as p_fd:
        json.dump(policy_graph, p_fd)

    return POLICY_GRAPH, json.dumps(policy_graph), new_policy, POLICY_MAP


def create_summary_and_graph(CURR_LANDMARKS, FACT_LIST, selected_facts = None):
    assert len(CURR_LANDMARKS) > 0, "Empty policy or empty model"
    summary_graph = {NODES:[], LINKS:[]}
    successors = {}
    immediate_successors = {}

    print ("CURR LAND", CURR_LANDMARKS)
    if selected_facts and 'goal_completed' not in selected_facts:
            selected_facts.append('goal_completed')

    if selected_facts and 'goal_achieved' not in selected_facts:
            selected_facts.append('goal_achieved')
    print("Selected facts", selected_facts)

    # TODO: Hack just add the initial state for now


    START_NODES = []
    for node in CURR_LANDMARKS:
        #if not selected_facts or node in selected_facts:
        if len(CURR_LANDMARKS[node]) == 0:
            START_NODES.append(node)
        successors[node] = []
        immediate_successors[node] = set()
        for poss_succ in CURR_LANDMARKS:
            if poss_succ != node and node in CURR_LANDMARKS[poss_succ]:
                #if not selected_facts or poss_succ in selected_facts:
                successors[node].append(poss_succ)
                immediate_successors[node].add(poss_succ)
    print ("CURR LAND", immediate_successors)

    converged = False
    while not converged:
        converged = True
        for node in successors:
            for succ_node in successors[node]:
                for further_succ in successors[succ_node]:
                    if further_succ not in successors[node]:
                        successors[node].append(further_succ)
                        converged = False
    end_nodes = Queue()
    closest_remaining_succ = {}
    for node in successors:
        if len(successors[node]) == 0:
            end_nodes.put(node)
            closest_remaining_succ[node] = set()

    filtered_landmarks = {}

    for node in START_NODES:
        if selected_facts and node not in selected_facts:
            selected_facts.append(node)

    if selected_facts:
        already_cleared_nodes = set()
        while not end_nodes.empty():
            curr_node = end_nodes.get()
            closest_remaining_succ[curr_node] = set()
            if curr_node in selected_facts:
                filtered_landmarks[curr_node] = set()
            for succ_node in immediate_successors[curr_node]:

                if succ_node not in selected_facts:
                    if curr_node in selected_facts:
                        for succ_node_2 in closest_remaining_succ[succ_node]:
                            filtered_landmarks[curr_node].add(succ_node_2)
                else:
                    if curr_node in selected_facts:
                        filtered_landmarks[curr_node].add(succ_node)
                    closest_remaining_succ[curr_node].add(succ_node)

            already_cleared_nodes.add(curr_node)

            for lnd in CURR_LANDMARKS[curr_node]:
                if immediate_successors[lnd] <= already_cleared_nodes:
                    end_nodes.put(lnd)
    else:
        filtered_landmarks = copy.deepcopy(immediate_successors)


    #TODO: Make a filtered list here

    #for node in CURR_LANDMARKS.keys():

    print (filtered_landmarks)

    successors[ALL_FACTS] = []
    curr_land_list = []
    for node in filtered_landmarks.keys():
        if not selected_facts or node in selected_facts:
            curr_land_list.append(node)
    successors[ALL_FACTS] = copy.deepcopy(FACT_LIST)


    for node in filtered_landmarks:
        if node not in summary_graph[NODES]:
            if len(CURR_LANDMARKS[node]) > 0:
                if len(successors[node]) == 0:
                    node_type = "goal"
                else:
                    node_type = "other"
            else:
                node_type = "init"

            summary_graph[NODES].append({DESCRIPTION:node, TYPE:node_type})

    for node in filtered_landmarks:
        if not selected_facts or node in selected_facts:
            for succ in filtered_landmarks[node]:
                try:
                    summary_graph[LINKS].append({TARGET: curr_land_list.index(node), SOURCE:curr_land_list.index(succ)})
                except ValueError as exc:
                    print ("missing index for ", exc)

    with open('./static/summary.json', 'w') as p_fd:
        json.dump(summary_graph, p_fd)

    with open('./static/successor.json', 'w') as p_fd:
        json.dump(successors, p_fd)



def get_succ_sets(start_state, goal_fact, POLICY_GRAPH):
    fringe_states = Queue()
    closed_set = set()
    fringe_states.put(start_state)
    closed_set.add(start_state)
    while not fringe_states.empty():
        new_state = fringe_states.get()
        #print ("####",new_state)

        closed_set.add(new_state)
        if goal_fact not in new_state.split(STATE_SEPARATOR):
            #print ("####poss_succ", POLICY_GRAPH[new_state][SUCCESSORS])
            for succ_state in POLICY_GRAPH[new_state][SUCCESSORS]:
                if succ_state not in closed_set:
                    fringe_states.put(succ_state)
    return closed_set


def get_policy_graph_fragment(start_state, goal_fact, CURR_POLICY,CURR_MODEL, POLICY_GRAPH, SELECTED_FACTS):
    #print ("###POLICY", POLICY_GRAPH)
    succ_node_set =  get_succ_sets(start_state, goal_fact, POLICY_GRAPH)
    POLICY_GRAPH, policy_json, policy_subset, POLICY_MAP = create_policy_graph(CURR_POLICY, CURR_MODEL, SELECTED_FACTS, succ_node_set, start_state, goal_fact)
    return POLICY_GRAPH, policy_json, policy_subset, POLICY_MAP

def get_avoid_proposition(fact):
    return AVOID_PROPOSITION + fact

def get_updated_model(CURR_MODEL, selected_fact):
    new_model = copy.deepcopy(CURR_MODEL)
    avoid_prop = get_avoid_proposition(selected_fact)
    new_model[INSTANCE][GOAL].add(avoid_prop)
    new_model[INSTANCE][INIT].add(avoid_prop)
    for act in new_model[DOMAIN]:
        if selected_fact in new_model[DOMAIN][act][ADDS]:
            new_model[DOMAIN][act][DELS].add(avoid_prop)
    return new_model

def test_unsolvability(CURR_MODEL, selected_fact, domain_name):
    new_model = get_updated_model(CURR_MODEL, selected_fact)
    d_fd, tmp_domain_file = tempfile.mkstemp()
    os.close(d_fd)
    p_fd, tmp_problem_file = tempfile.mkstemp()
    os.close(p_fd)
    write_det_model(new_model, domain_name, tmp_domain_file, tmp_problem_file)
    # Run the planner
    plan = [act.strip() for act in os.popen(PLANNING_CMD.format(tmp_domain_file, tmp_problem_file)).read().split()]
    os.remove(tmp_domain_file)
    os.remove(tmp_problem_file)
    if len(plan) > 0:
        return False
    return True
