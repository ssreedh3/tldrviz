#from .constants import *
from psummarizer.constants import *
from psummarizer.model_parser import parse_model_str
from psummarizer.utils import parse_policy_str, propositionalize_fluent, remove_init_and_goal_landmarks, parse_policy_list
from psummarizer.concise_compiler import ConciseCompiler
from psummarizer.FD_landmark_extractor import dump_all_landmarks_without_ordering_from_map, dump_all_landmarks_without_ordering_from_files, dump_landmarks_with_ordering
from json import JSONEncoder, JSONDecoder
from flask import Flask, request, jsonify, json
import yaml
app = Flask(__name__)


@app.route('/subgoals', methods=["GET"])
def get_subgoals():
    if request.method  == ["GET"]:
        assert hasattr(request.args, 'policy')
        policy = request.args.policy
        assert hasattr(request.args, 'domain')
        domain = request.args.domain
        assert hasattr(request.args, 'problem')
        problem = request.args.problem

        # Determinize the domain
    return ""


@app.route('/subgoals_from_map', methods=["GET"])
def get_subgoals_det():
    policy_landmarks_all = {}
   # if request.method == ["GET"]:
    #print (request.args)
    assert 'policy' in request.args
    policy = yaml.load(request.args['policy'])
    assert 'det_model_map' in request.args
    map_yaml = request.args['det_model_map']

    #print (policy_list)
    # get subgoal
    # Parse the model
    model_dict = yaml.load(map_yaml)
    # Parse the policy
    #policy = parse_policy_list(policy_list)

    # Create a concise compiled model
    compiler = ConciseCompiler()
    print ("goal det model", model_dict[INSTANCE][GOAL])
    print ("Creating compiled model")
    compiled_model = compiler.generate_full_compiled_model(model_dict, policy, allow_goal_act=True)
    print ("Creating landmarks")
    policy_landmarks_all = dump_landmarks_with_ordering(compiled_model)
    # Remove parent
    dedup_map = {}
    for node in policy_landmarks_all:
        grand_pars = set()
        for k in policy_landmarks_all[node]:
            for j in policy_landmarks_all[k]:
                if j!= k:
                    grand_pars.add(j)
        dedup_map[node] = policy_landmarks_all[node] - grand_pars
    print ("landmarks", dedup_map)
    return yaml.dump(dedup_map)


if __name__ == "__main__":
    app.run(debug=True, port=8080)