begin_version
3
end_version
begin_metric
0
end_metric
20
begin_variable
var0
-1
2
Atom truck1_at_stop2()
NegatedAtom truck1_at_stop2()
end_variable
begin_variable
var1
-1
2
Atom package_in_truck1()
NegatedAtom package_in_truck1()
end_variable
begin_variable
var2
-1
2
Atom truck1_at_stop4()
NegatedAtom truck1_at_stop4()
end_variable
begin_variable
var3
-1
2
Atom truck1_at_stop5()
NegatedAtom truck1_at_stop5()
end_variable
begin_variable
var4
-1
2
Atom truck1_at_stop6()
NegatedAtom truck1_at_stop6()
end_variable
begin_variable
var5
-1
2
Atom truck1_at_stop3()
NegatedAtom truck1_at_stop3()
end_variable
begin_variable
var6
-1
2
Atom truck1_at_port1()
NegatedAtom truck1_at_port1()
end_variable
begin_variable
var7
-1
2
Atom package_in_barge()
NegatedAtom package_in_barge()
end_variable
begin_variable
var8
-1
2
Atom barge_in_port3()
NegatedAtom barge_in_port3()
end_variable
begin_variable
var9
-1
2
Atom package_in_truck3()
NegatedAtom package_in_truck3()
end_variable
begin_variable
var10
-1
2
Atom truck3_at_stop8()
NegatedAtom truck3_at_stop8()
end_variable
begin_variable
var11
-1
2
Atom truck3_at_stop9()
NegatedAtom truck3_at_stop9()
end_variable
begin_variable
var12
-1
2
Atom truck3_at_factory()
NegatedAtom truck3_at_factory()
end_variable
begin_variable
var13
-1
2
Atom barge_in_port2()
NegatedAtom barge_in_port2()
end_variable
begin_variable
var14
-1
2
Atom package_in_truck2()
NegatedAtom package_in_truck2()
end_variable
begin_variable
var15
-1
2
Atom truck2_at_stop7()
NegatedAtom truck2_at_stop7()
end_variable
begin_variable
var16
-1
2
Atom truck2_at_stop9()
NegatedAtom truck2_at_stop9()
end_variable
begin_variable
var17
-1
2
Atom truck2_at_factory()
NegatedAtom truck2_at_factory()
end_variable
begin_variable
var18
-1
2
Atom package_in_factory()
NegatedAtom package_in_factory()
end_variable
begin_variable
var19
-1
2
Atom goal_achieved()
NegatedAtom goal_achieved()
end_variable
0
begin_state
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
end_state
begin_goal
1
19 0
end_goal
21
begin_operator
act0 
0
1
0 0 -1 0
1
end_operator
begin_operator
act1 
4
13 0
18 0
6 0
17 0
1
0 19 -1 0
1
end_operator
begin_operator
act10 
4
8 0
18 0
6 0
12 0
1
0 19 -1 0
1
end_operator
begin_operator
act11 
2
1 0
0 0
2
0 5 -1 0
0 2 -1 0
1
end_operator
begin_operator
act12 
2
1 0
2 0
1
0 3 -1 0
1
end_operator
begin_operator
act13 
2
1 0
3 0
1
0 4 -1 0
1
end_operator
begin_operator
act14 
3
13 0
7 0
6 0
1
0 14 -1 0
1
end_operator
begin_operator
act15 
1
0 0
1
0 1 -1 0
1
end_operator
begin_operator
act16 
3
8 0
9 0
6 0
1
0 10 -1 0
1
end_operator
begin_operator
act17 
2
1 0
6 0
1
0 7 -1 0
1
end_operator
begin_operator
act18 
2
7 0
6 0
2
0 13 -1 0
0 8 -1 0
1
end_operator
begin_operator
act19 
4
8 0
9 0
6 0
11 0
1
0 12 -1 0
1
end_operator
begin_operator
act2 
4
13 0
14 0
6 0
16 0
1
0 17 -1 0
1
end_operator
begin_operator
act20 
3
8 0
7 0
6 0
1
0 9 -1 0
1
end_operator
begin_operator
act3 
4
8 0
9 0
6 0
12 0
1
0 18 -1 0
1
end_operator
begin_operator
act4 
4
13 0
14 0
6 0
17 0
1
0 18 -1 0
1
end_operator
begin_operator
act5 
4
13 0
14 0
6 0
15 0
1
0 16 -1 0
1
end_operator
begin_operator
act6 
3
13 0
14 0
6 0
1
0 15 -1 0
1
end_operator
begin_operator
act7 
2
1 0
5 0
1
0 6 -1 0
1
end_operator
begin_operator
act8 
4
8 0
9 0
6 0
10 0
1
0 11 -1 0
1
end_operator
begin_operator
act9 
2
1 0
4 0
1
0 6 -1 0
1
end_operator
0
