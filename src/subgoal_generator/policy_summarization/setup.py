#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    author="Sarath Sreedharan",
    author_email="ssreedh3@asu.edu",
    include_package_data=True,
    license="TBD",
    name="Policy Summarization",
    packages=find_packages("src", exclude=["tests*"]),
    package_dir={"": "src"},
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
)

