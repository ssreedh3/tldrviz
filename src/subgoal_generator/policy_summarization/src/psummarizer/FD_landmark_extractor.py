from psummarizer.constants import *
from psummarizer.model_writer import ModelWriter
import tempfile
import os
import re

# Find location of the current script
DUMMY_LANDMARK = "dummy"
DEBUG = True
#LANDMARK_GEN_SCRIPT = os.path.join(SRC_DIR, 'generate_landmarks_only.sh')
LANDMARK_GEN_SCRIPT = os.path.join(SRC_DIR, 'generate_landmarks.sh')
FD_LANDONLY_COMMAND = LANDMARK_GEN_SCRIPT + " {} {} {}"
#FD_LAND_AND_ORDER_COMMAND = LANDMARK_GEN_SCRIPT + " {} {} {}"

def extract_landmarks_only(domain_file, instance_file):
    lfd, landmark_file = tempfile.mkstemp()
    landmark_dump_command = FD_LANDONLY_COMMAND.format(domain_file, instance_file, landmark_file)
    landmark_extraction_status = [i.strip() for i in os.popen(landmark_dump_command).read().strip().split('\n')]
    all_landmarks = set()
    with open(landmark_file) as l_fd:
        all_landmarks = set([land.strip() for land in l_fd.readlines()])
    return all_landmarks

def parse_land_line(orig_line, relative = False):
    if 'conj' in orig_line and 'NegatedAtom'  in orig_line:
        print ("Not modelled")
        exit(1)

    line = re.sub('^LM \d Atom','StAtom',orig_line)
    #line = re.sub('^LM \d NegatedAtom','StngAtom',orig_line)
    PARSE_STR = "Atom "
    CONJ_STR = "CONJ"
    if 'conj {Atom ' not in line:
        raw_atom = line.split(PARSE_STR)[-1].split(' (')[0].replace('(',' ').replace(', ',' ').replace(',',' ').replace(')','').strip()
        if "NegatedAtom" in line:
            atom = "not ("+raw_atom+")"
        else:
            atom = raw_atom
    else:
        print ("Doesn't support conjunctions")
        exit(1)

    ld_type = "curr"
    if relative:
        if 'gn ' in line:
            ld_type = "gn"
        else:
            ld_type = "nat"
    return (atom, ld_type)

def parse_landmark_file(lfile):
    with open(lfile) as l_fd:
        land_content = [re.sub(r'var\d+\(\d+\)->\d+', '', l.strip()) for l in l_fd.readlines()]

    if land_content == [""]:
        return {}
    ind = 0
    parent_map = {}
    while ind < len(land_content):
        new_land = []
        curr_atom, ld_type = parse_land_line(land_content[ind])
        if curr_atom not in parent_map and DUMMY_LANDMARK not in curr_atom:
            parent_map[curr_atom] = set() #{'gn':set(), 'nat':set()}
        ind += 1
        while ind < len(land_content) and ('<-' in land_content[ind] or '->' in land_content[ind]):
            #print (land_content[ind])
            next_atom, order_type = parse_land_line(land_content[ind], relative=True)
            if '<-' in land_content[ind]:
                parent_map[curr_atom].add(next_atom)
            ind+=1

    return parent_map


def extract_landmarks_and_ordering(domain_file, instance_file):
    #TODO:
    if not DEBUG:
        (p_fd, landmark_file) = tempfile.mkstemp()
    else:
        landmark_file = "/tmp/viz_landmark"
    landmark_dump_command = FD_LANDONLY_COMMAND.format(domain_file, instance_file, landmark_file)
    landmark_extraction_status = [i.strip() for i in os.popen(landmark_dump_command).read().strip().split('\n')]
    landmarks = parse_landmark_file(landmark_file)
    if not DEBUG:
        os.remove(landmark_file)
    return landmarks

def dump_all_landmarks_without_ordering_from_map(model):
    # Get the domain and problem file
    curr_writer = ModelWriter(model)
    (d_fd, tmp_domain_file) = tempfile.mkstemp()
    (p_fd, tmp_problem_file) = tempfile.mkstemp()
    curr_writer.write_files(tmp_domain_file, tmp_problem_file)
    landmarks = extract_landmarks_only(tmp_domain_file, tmp_problem_file)
    os.remove(tmp_domain_file)
    os.remove(tmp_problem_file)
    return landmarks

def dump_all_landmarks_without_ordering_from_files(domain_file, problem_file):
    # Get the domain and problem file
    landmarks = extract_landmarks_only(domain_file, problem_file)
    return landmarks

def dump_landmarks_with_ordering(model):
    # Get the domain and problem file
    curr_writer = ModelWriter(model)
    if not DEBUG:
        (d_fd, tmp_domain_file) = tempfile.mkstemp()
        (p_fd, tmp_problem_file) = tempfile.mkstemp()
    else:
        tmp_domain_file = "/tmp/viz_domain.pddl"
        tmp_problem_file = "/tmp/viz_problem.pddl"
    curr_writer.write_files(tmp_domain_file, tmp_problem_file)
    landmarks = extract_landmarks_and_ordering(tmp_domain_file, tmp_problem_file)
    if not DEBUG:
        os.remove(tmp_domain_file)
        os.remove(tmp_problem_file)
    return landmarks
