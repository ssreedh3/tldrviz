import copy
from psummarizer.compiler import Compiler
from psummarizer.constants import *
from psummarizer.utils import test_condition, ground_action, propositionalize_fluent

# The upper limit for number of determinized action there can be
MAX_ACT_DETERMINIZATION_LIMIT = 100
ACT_DELIM = ' '

class ConciseCompiler(Compiler):
    def __init__(self):
        pass

    def find_var_to_obj_map(self, action_parts, params):
        map = {}
        assert len(params) == len(action_parts)-1,\
            "The number of arguments {} passed do not match the grounded actions parts {}".\
                format(str(len(params)), str(len(action_parts)-1))
        for i in range(len(params)):
            map[params[i]] = action_parts[i+1]
        return map

    def get_ground_action_without_conditional(self, act_def, act_name, state):
        #TODO : Pass it through for now
        # find the mapping from parameters to objects from the action name
        # Call the ground action method to return a grounded action
        # For conditional effects find out what are applicable and add it
        # to the add or delete effect

        parameters = act_def.get(PARARMETERS, [])
        var_to_obj_map = self.find_var_to_obj_map(act_name.split(ACT_DELIM), parameters)
        grounded_def_with_cond = ground_action(act_def, var_to_obj_map)
        flattened_def = {}

        for def_part in grounded_def_with_cond:
            if def_part in [COND_ADDS, COND_DELS]:
                target_key = ADDS if def_part == COND_ADDS else COND_DELS
                if target_key not in flattened_def:
                    flattened_def[target_key] = set()
                for cond, eff in grounded_def_with_cond[def_part]:
                    if test_condition(state, cond):
                        flattened_def[target_key].add(eff)
            else:
                flattened_def[def_part] = set()
                for prop in grounded_def_with_cond[def_part]:
                    flattened_def[def_part].add(prop)
        return flattened_def

    def get_operatory_name(self, act_name):
        # Assume the objects are added to the operator name by space
        return act_name.split(' ')[0]

    def generate_full_compiled_model(self, original_model, policy, allow_goal_act = False):
        compiled_model = {DOMAIN:{}, INSTANCE:{}}
        compiled_model[INSTANCE][INIT] = set([propositionalize_fluent(fl) for fl in copy.deepcopy(original_model[INSTANCE][INIT])])
        if not allow_goal_act:
            compiled_model[INSTANCE][GOAL] = copy.deepcopy(original_model[INSTANCE][GOAL])
        else:
            compiled_model[INSTANCE][GOAL] = set([GOAL_ACHIEVED,])

        curr_act_id = 0
        act_prefix = 'act'
        for state, act_name in policy:
            operator_name = self.get_operatory_name(act_name)
            compiled_model[DOMAIN][act_prefix + str(curr_act_id)] = {}
            compiled_model[DOMAIN][act_prefix + str(curr_act_id)][POS_PREC] = copy.deepcopy(state)
            if allow_goal_act and operator_name == GOAL_ACT:
                compiled_model[DOMAIN][act_prefix + str(curr_act_id)][ADDS] = set([GOAL_ACHIEVED,])
            else:
                # TODO: Here assumption is that the model is determinized and named as act+det_id
                det_id = 0
                compiled_model[DOMAIN][act_prefix + str(curr_act_id)][ADDS] = set()
                while operator_name+str(det_id) in original_model[DOMAIN]:
                    if det_id > MAX_ACT_DETERMINIZATION_LIMIT:
                        raise Exception("Too many determinized actions, please raise MAX_ACT_DETERMINIZATION_LIMIT")

                    tmp_act = self.get_ground_action_without_conditional(original_model[DOMAIN][operator_name+str(det_id)],
                                                                         act_name, state)
                    compiled_model[DOMAIN][act_prefix + str(curr_act_id)][ADDS] |= set([propositionalize_fluent(fl) for fl in tmp_act[ADDS]])
                    det_id += 1
            curr_act_id += 1

        return compiled_model
