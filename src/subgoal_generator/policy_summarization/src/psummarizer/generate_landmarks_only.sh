#!/bin/bash 

#RADAR_REPO="/media/data_mount/mycode/RADAR/FD/src"
#TODO: Move this to the current repo
LANDMARK_FD_REPO="/home/local/ASUAD/ssreedh3/mycode/FD_LAND_DUMP"
dom_file=$1
prob_file=$2
land_file=$3

# Dangerous if land_file is empty
#rm $land_file 
# Assume fact landmarks and the atom is always on third column and is always positive
# First extract all lines that start with LM
# Then remove the beggining of the line upto Atom \d+
# Then remove everying thing after the first closing peranthesis
# Sample line:
# LM 2 Atom on(crate0, crate3) (var0(0)->8) Achievers (9, 9)

python ${LANDMARK_FD_REPO}/fast-downward.py --build "release64"   $dom_file $prob_file  --search "astar(lmcount(lm_factory=lm_hm(m=1)))"|grep -E "^LM "|sed 's/.*Atom //'|sed 's/).*/)/'|sed 's/(/ /'|sed 's/,//g'|sed 's/)//' > ${land_file}

cp $1 /tmp/domain.pddl
cp $2 /tmp/problem.pddl

python ${LANDMARK_FD_REPO}/fast-downward.py --build "release64"   $dom_file $prob_file  --search "astar(lmcount(lm_factory=lm_hm(m=1)))"|grep -E "LM " > /tmp/landmark.txt
#|sed 's/.*Atom //'|sed 's/).*/)/'|sed 's/(/ /'|sed 's/,//'|sed 's/)//' > /tmp/landmark.txt


#python ${RADAR_REPO}/fast-downward.py $dom_file $prob_file  --landmarks name=lm_zg > /dev/null

#cat landmark.txt #|sed 's/,//'|sed 's/(/ /'|sed 's/^/(/'|sed 's/ )/)/' #|sed 's/.*Atom //'|sed 's/,//'|sed 's/(/ /'|sed 's/^/(/'|sed 's/ )/)/'


