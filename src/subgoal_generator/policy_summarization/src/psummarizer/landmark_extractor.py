import sys
import copy

MAX_UNROLL_LIMIT = 100
natural_ordering_list = {}
greedy_necc_ordering_list = {}
# TODO: Problem list -> Adding conjunctions make things too slow
# TODO:
# 1. Don't worry about the ordering for now -- Done
# 2. Don't worry about conjuncts for now -- Done
# 3. Fix recursion bug
# 4. Remove ununsed params
# 5. Extend to handle disjunctions

def get_conjunct_key(fact_set):
    return frozenset(fact_set)

def intersect_landmarks(land1, land2, allow_conjunctions = False):
    if not allow_conjunctions:
        return land1 & land2
    fact_set_1 = set()
    fact_set_2 = set()

    for conj in land1:
        for l_fc in conj:
            fact_set_1.add(l_fc)

    for conj in land2:
        for l_fc in conj:
            fact_set_2.add(l_fc)

    remaining_facts = fact_set_1 & fact_set_2
    tmp_intersect = set()
    for conj in land1|land2:
        rem_conj = remaining_facts & conj
        if len(rem_conj) > 0:
            tmp_intersect.add(frozenset(rem_conj))

    intersect = set()
    for conj in tmp_intersect:
        no_superset = False
        for c2 in tmp_intersect:
            if c2 > conj:
                no_superset = True
        if not no_superset:
            intersect.add(conj)
    return intersect


# Union for actions
def get_union_of_landmarks(act, model, label_map, allow_conjunctions = False):
    '''

    '''
    # Standard method would involve finding the union of landmark for every fact in the precondition
    # Here if action is not part of greedy_necc_ordering_map add a mapping from action to precondition set
    #print ("Union started ...")

    curr_set = set()
    if allow_conjunctions:
        curr_set.add(get_conjunct_key(model[act]['precondition']))
    for f in model[act]['precondition']:
        if not allow_conjunctions:
            curr_set.add(frozenset([f]))
        if f in label_map:
            labels = label_map[f]
            curr_set |= labels
        else:
            print ("Missing labels for fact",f)
            exit(1)

    return curr_set


# Intersection for facts
def get_intersection_of_landmarks(fact, act_set, act_map, allow_conjunctions=False):
    '''

    '''
    global natural_ordering_list

    current_labels = set([frozenset([fact])])

    act_id = 0
    labels = set()
    if len(act_set) > 0:
        act_list =list(act_set)
        act = act_list[0]
        labels = act_map[act]
        for act in act_list[1:]:
            act_id += 1
            labels = intersect_landmarks(labels, act_map[act], allow_conjunctions)
            # if frozenset([fact]) in labels:
            #     print("There is loops", fact, act, labels)
            #     exit(0)
    # There should be no reason to ever do this but :(

    if fact not in labels:
        greedy_necc_ordering_list[fact] = labels
    #natural_ordering_list[fact] = labels - set([frozenset(fact)])

    return current_labels|labels

def get_action_str(act_name, precondition, add_effs):
    return "(:action {} :parameters () :precondition (and {}) :effect (and {}))".format(act_name,"\n".join(['('+pred +')' for pred in precondition]), "\n".join(['('+pred +')' for pred in add_effs]))

# Set the landmarks
def get_landmarks(init, goal, model, allow_conjunctions = False):
    '''
       :params init - A set for initial state
       :params goal - Goal specifications set
       :params model - A dictionary containing grounded action precondition and effects
    '''
    current_state = init

    fixed_point_reached = False
    curr_level = 1

    label_map = {}

    # write the model into a file
    act_str_list = []

    for act in model:
        act_str_list.append(get_action_str(act,  model[act]['precondition'], model[act]['add_effects']))

    # with open('/tmp/test.pddl','w') as t_fd:
    #     t_fd.write("\n".join(act_str_list))

    print ("Extract the relaxed planning graph")
    for fact in current_state:
        label_map[fact] = set([frozenset([fact])])

    action_labels = {}
    while not fixed_point_reached and curr_level <= MAX_UNROLL_LIMIT:
        current_layer = {}
        for fc in init:
            current_layer[fc] = set()

        # Look for applicable action
        new_state = copy.deepcopy(current_state)
        curr_level += 1
        for act in model:
            preconditions = model[act]['precondition']
            if preconditions <= current_state:
                for fact in model[act]['add_effects']:
                    new_state.add(fact)
                    if fact not in current_layer:
                        current_layer[fact] = set()
                    current_layer[fact].add(act)
                action_labels[act] = get_union_of_landmarks(act, model, label_map, allow_conjunctions)
        if new_state == current_state:
            fixed_point_reached = True

        current_state = copy.deepcopy(new_state)
        new_label_map = {}
        print ("Layer", curr_level, len(current_layer))
        f_ind = 0
        for fact in current_layer:
            print (f_ind, len(current_layer[fact]))
            new_label_map[fact] = get_intersection_of_landmarks(fact, current_layer[fact], action_labels, allow_conjunctions)
            f_ind += 1
        label_map = copy.deepcopy(new_label_map)
    print ("Planning graph extraction done...")
    greedy_necc_ordering_list = {}
    for fact in label_map:
        greedy_necc_ordering_list[fact] = set()
        for pred in label_map[fact]:
            pred_fact = list(pred)[0]
            if frozenset([fact]) not in label_map[pred_fact]:
                greedy_necc_ordering_list[fact].add(pred)

    # Extract the ordered landmark list
    # TODO: Invert it
    all_found_facts = set()
    landmark_layers = [set()]
    for in_fact in init:
        landmark_layers[-1].add((frozenset([in_fact])))
        all_found_facts.add(frozenset([in_fact]))
    prev_pred = copy.deepcopy(label_map[list(goal)[0]])
    while len(prev_pred) > 0:
        org_cnt = len(prev_pred)
        print (org_cnt)
        curr_pred = set()

        for lnd_conj in prev_pred:
            lnd_fact = list(lnd_conj)[0]
            if (greedy_necc_ordering_list[lnd_fact] - set([lnd_conj])) <= all_found_facts:
                    # Why is this happening
                    #if lnd_conj != prev_lnd_conj:
                    curr_pred |= set([lnd_conj])
        #print(curr_pred)

        landmark_layers.append(curr_pred)
        all_found_facts |= curr_pred
        prev_pred = prev_pred - curr_pred
        #print(all_found_facts)

        if len(prev_pred) == org_cnt:
            print ("Loopdy loop")
            for pred in prev_pred:
                pred_fc = list(pred)[0]
                print (pred, label_map[pred_fc]-all_found_facts)
            exit(1)
    # Assume a single goal predicate
    print (landmark_layers)

    exit(0)
    return label_map[list(goal)[0]], list(reversed(landmark_layers))
