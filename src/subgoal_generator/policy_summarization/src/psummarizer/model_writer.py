from psummarizer.constants import *
from tarski import fstrips as fs
from tarski.fstrips.problem import create_fstrips_problem
from tarski.fstrips import language
from tarski.syntax import land, top

from tarski.io.fstrips import FstripsWriter

class ModelWriter(object):
    def __init__(self, model):
        self.model_dict = model
        self.fstrips_problem = create_fstrips_problem(language(),"test_domain", "instance1")
        self.fluent_set_map = {}
        self.populate_fstrips_object()

    def add_predicates_to_the_prob(self, fluent):
        # TODO: Assuming only propositional models
        if fluent not in self.fluent_set_map:
            # Add to tarski list
            pred_obj = self.fstrips_problem.language.predicate(fluent)
            self.fluent_set_map[fluent] = pred_obj
        return self.fluent_set_map[fluent]

    def add_types_to_the_prob(self, var, type):
        pass

    def get_conjunctions(self, fluent_list):
        if len(fluent_list) == 0:
            return top
        if len(fluent_list) <= 1:
            return self.add_predicates_to_the_prob(list(fluent_list)[0])()
        else:
            try:
                return land(*[self.add_predicates_to_the_prob(fl)() for fl in fluent_list])
            except AssertionError as exc:
                raise Exception("Message:",exc," Original fluent set", fluent_list)

    def populate_fstrips_object(self):
        # Populate initial state
        for init_val in self.model_dict[INSTANCE][INIT]:
            self.add_predicates_to_the_prob(init_val)
            self.fstrips_problem.init.add(self.add_predicates_to_the_prob(init_val))
        # populate goal state
        print ("GOA>>",self.model_dict[INSTANCE][GOAL])
        self.fstrips_problem.goal = self.get_conjunctions(self.model_dict[INSTANCE][GOAL])
        # populate action models
        for act in self.model_dict[DOMAIN]:
            act_name = act
            # Only pos
            precond = self.get_conjunctions(self.model_dict[DOMAIN][act][POS_PREC])
            add_effects = [fs.AddEffect(self.add_predicates_to_the_prob(fl)()) for fl in self.model_dict[DOMAIN][act].get(ADDS,set())]
            delete_effects = [fs.DelEffect(self.add_predicates_to_the_prob(fl)())
                              for fl in self.model_dict[DOMAIN][act].get(DELS,set())]
            if PARARMETERS in self.model_dict[DOMAIN][act]:
                pars = self.model_dict[DOMAIN][act][PARARMETERS]
            else:
                pars = []
            self.fstrips_problem.action(act_name, pars, precond, add_effects + delete_effects)

    def write_files(self, domain_file, problem_file):
        curr_writer = FstripsWriter(self.fstrips_problem)
        curr_writer.write(domain_file, problem_file)

