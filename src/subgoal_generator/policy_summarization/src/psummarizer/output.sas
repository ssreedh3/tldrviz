begin_version
3
end_version
begin_metric
0
end_metric
3
begin_variable
var0
-1
2
Atom p()
NegatedAtom p()
end_variable
begin_variable
var1
-1
2
Atom q()
NegatedAtom q()
end_variable
begin_variable
var2
-1
2
Atom g()
NegatedAtom g()
end_variable
0
begin_state
1
1
1
end_state
begin_goal
1
2 0
end_goal
5
begin_operator
a0 
0
1
0 0 -1 0
1
end_operator
begin_operator
b0 
1
0 0
1
0 1 -1 0
1
end_operator
begin_operator
c0 
0
1
0 1 -1 0
1
end_operator
begin_operator
d0 
1
1 0
1
0 0 -1 0
1
end_operator
begin_operator
g0 
1
1 0
1
0 2 -1 0
1
end_operator
0
