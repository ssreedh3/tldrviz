import re
import copy
import json
from psummarizer.constants import *

def parse_policy_lines(policy_line):
    policy_line_reg = re.compile("\[(.*)\] \((.*)\)")
    state_str = policy_line_reg.match(policy_line).group(1)
    act = policy_line_reg.match(policy_line).group(2)
    state_set = set()
    for pred in state_str.split(' ,'):
        if pred != '':
            state_set.add(pred.replace('(','').replace(')','').strip().replace(' ','_'))
    return act, state_set


def parse_policy(policy_file):
    # policy list consists of lists of the form
    # [state_set, action]
    policy_list = []
    with open(policy_file) as p_fd:
        for line in p_fd.readlines():
            act, state = parse_policy_lines(line)
            policy_list.append([state, act])
    return policy_list

def parse_policy_str(policy_str):
    # policy list consists of lists of the form
    # [state_set, action]
    policy_list = []

    raw_policy_list = policy_str.split("\n")

    for line in raw_policy_list:
        act, state = parse_policy_lines(line)
        policy_list.append([state, act])
    return policy_list

def parse_policy_list(raw_policy_list):
    # policy list consists of lists of the form
    # [state_set, action]
    policy_list = []

    for line in raw_policy_list:
        act, state = parse_policy_lines(line)
        policy_list.append([state, act])
    return policy_list

def ground_predicate(orig_pred, var_map):
    new_pred = orig_pred
    for var in var_map:
        #Add an escape character to ?
        new_pred = new_pred.replace(var, var_map[var])
    return new_pred


def ground_action(action_map, var_map):
    grounded_action = copy.deepcopy(action_map)

    # ground preconditions
    if POS_PREC in action_map:
        grounded_action[POS_PREC] = set([ground_predicate(pred, var_map) for pred in action_map[POS_PREC]])

    # ground conditional add effects
    if COND_ADDS in action_map:
        grounded_action[COND_ADDS] = []

        for cond, eff in action_map[COND_ADDS]:
            grounded_cond = [ground_predicate(pred, var_map) for pred in cond]
            grounded_eff = ground_predicate(eff, var_map)
            grounded_action[COND_ADDS].append([grounded_cond, grounded_eff])

    if COND_DELS in action_map:
        # ground conditional delete effects
        grounded_action[COND_DELS] = []
        for cond, eff in action_map[COND_DELS]:
            grounded_cond = [ground_predicate(pred, var_map) for pred in cond]
            grounded_eff = ground_predicate(eff, var_map)
            grounded_action[COND_DELS].append([grounded_cond, grounded_eff])

    # ground add effects
    if ADDS in action_map:
        grounded_action[ADDS] = set([ground_predicate(pred, var_map) for pred in action_map[ADDS]])

    # ground delete effects
    if DELS in action_map:
        grounded_action[DELS] = set([ground_predicate(pred, var_map) for pred in action_map[DELS]])


    return grounded_action


def test_condition(state, condition):
    non_equality_preds = set()
    for pred in condition:
        if '=' in pred:
            pred_parts = pred.split(' ')
            assert len(pred_parts) == 3
            if pred_parts[1] != pred_parts[2]:
                return False
        else:
            non_equality_preds.add(pred)
    if non_equality_preds <= state:
        return True
    return False

def propositionalize_fluent(fluent):
    return fluent.replace(' ','_')


def remove_init_and_goal_landmarks(landmarks, init, goal):
    return (landmarks - init) - goal
